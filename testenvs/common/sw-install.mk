include ../testenv-common.mk

SHELL=/bin/bash

# Define the NSO container name once
export NSO_CNT=$(CNT_PREFIX)-nso

VRNETLAB_IMAGE:=gitlab.dev.terastrm.net:4567/terastream/vrnetlab/$(VRNETLAB_TAG)

# Enable IPv6 per default, set to false to disable (in Makefile, not here)
IPV6?=true
# start: start the test environment in a configuration that allows
# Python Remote Debugging. Exposes port 5678 on a random port on localhost.
# Per default, the IPv6 prefix is a randomly generated IPv6 network prefix in
# the ULA address space. Override by setting IPV6_NET variable to e.g.:
# IPV6_NET=2001:db8:1234:456:  # which becomes 2001:db8:1234:456::/64
# If the IPv6 prefix is not in the public unicast space (2000::/3), the IPv6
# default route will be removed. This makes it possible to have local IPv6
# connectivity within the network, yet not slow down attempts to reach Internet
# resources (where IPv6 would normally be tried first, and fail due to
# non-public addresses).
start:
	docker network inspect $(CNT_PREFIX) >/dev/null 2>&1 || docker network create $(CNT_PREFIX) $(shell [ "$(IPV6)" = "true" ] && LC_CTYPE=C && echo --ipv6 --subnet $${IPV6_NET:-fd00:$$(< /dev/urandom tr -dc a-f0-9 | head -c4):$$(< /dev/urandom tr -dc a-f0-9 | head -c4):$$(< /dev/urandom tr -dc a-f0-9 | head -c4):}:/64)
	docker volume create --label com.cisco.nso.testenv.name=$(CNT_PREFIX) $(CNT_PREFIX)-sw
	docker run -td --name $(NSO_CNT) --network-alias nso $(DOCKER_NSO_ARGS) -v $(CNT_PREFIX)-sw:/sw-images $(IMAGE_PATH)$(PROJECT_NAME)/testnso:$(DOCKER_TAG)
	docker run --name $(CNT_PREFIX)-sw-img -d -v $(CNT_PREFIX)-sw:/sw-images $(DOCKER_ARGS) gitlab.dev.terastrm.net:4567/terastream/software-images/test:latest
	docker run -d --name $(CNT_PREFIX)-dut --privileged $(DOCKER_ARGS) --network-alias dut $(VRNETLAB_IMAGE) --trace $(VRNETLAB_OPTS)
	-[ "$(IPV6)" = "true" ] && echo $$(docker network inspect --format '{{range .IPAM.Config}}{{- if (gt (split .Subnet ":"|len) 1) -}}{{.Subnet}}{{- end}}{{end}}' $(CNT_PREFIX)) | egrep "^[23]...:" || (echo "Removing IPv6 default route" && docker ps -aq --filter label=com.cisco.nso.testenv.name=$(CNT_PREFIX) | $(XARGS) -I CNT -n1 docker run --rm --net=container:CNT --cap-add=NET_ADMIN $(NSO_IMAGE_PATH)cisco-nso-base:$(NSO_VERSION) ip -6 route del default >/dev/null 2>&1 || true)

	$(MAKE) wait-started-nso

create-device:
	@echo "-- Add device to NSO"
	@echo "   Get the package-meta-data.xml file from the compiled NED (we grab it from the netsim build)"
	mkdir -p tmp
	docker cp $(CNT_PREFIX)-nso:/var/opt/ncs/packages/$(NED_NAME)/package-meta-data.xml tmp/package-meta-data.xml
	@echo "   Fill in the device-type in add-device.xml by extracting the relevant part from the package-meta-data of the NED"
	echo $(NSO_VERSION) | grep "^4" && xmlstarlet sel -N x=http://tail-f.com/ns/ncs-packages -t -c "//x:ned-id" tmp/package-meta-data.xml | grep cli && STRIP_NED=' -d "//x:ned-id" '; \
		xmlstarlet sel -R -N x=http://tail-f.com/ns/ncs-packages -t -c "//*[x:ned-id]" -c "document('../common/ncs-config-dev.xml')" tmp/package-meta-data.xml | xmlstarlet edit -O -N x=http://tail-f.com/ns/ncs-packages -N y=http://tail-f.com/ns/ncs -d "/x:xsl-select/*[x:ned-id]/*[not(self::x:ned-id)]" -m "/x:xsl-select/*[x:ned-id]" "/x:xsl-select/y:devices/y:device/y:device-type" $${STRIP_NED} | tail -n +2 | sed '$$d' | cut -c 3- > tmp/add-device.xml
	$(MAKE) loadconf FILE=tmp/add-device.xml
	../cmd.py "request devices fetch-ssh-host-keys" --success-pattern "result updated" --retry --time-limit 120
	../cmd.py "request devices sync-from" --success-pattern "result true" --retry --time-limit 60

create-software-pack:
	@echo "-- Add software-pack configuration to NSO"
	$(MAKE) loadconf FILE=ncs-config-software-pack.xml
	$(MAKE) loadconf FILE=../common/ncs-config-disable-confirm-steps.xml

create-request:
	$(MAKE) runcmdJ CMD="request devices device dut software-pack create-request"
	$(MAKE) runcmdJ CMD="show devices device dut software-pack"

execute-request:
	../cmd.py 'request devices device dut software-pack request 1 execute-request' --success-pattern 'status enqueued'
	../cmd.py 'show devices device dut software-pack' --success-pattern 'status\s+done' --retry --time-limit 3600 --on-fail 'show devices device dut software-pack request 1 run-log | tab'
	../cmd.py 'show devices device dut software-pack request 1 run-log | tab'

test:
	$(MAKE) create-device
	if $(MAKE) -n extra-config >/dev/null 2>&1; then $(MAKE) extra-config; fi
	$(MAKE) create-software-pack
	$(MAKE) create-request
	$(MAKE) execute-request
	$(MAKE) verify-version

.PHONY: start test create-device create-software-pack create-request execute-request
