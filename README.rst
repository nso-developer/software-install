software-install
================

The software install package provides a declarative interface for installing
(upgrading and downgrading) operating system and patches on network devices.

Currently supported platforms:

- Cisco IOS XR

  - 6.5+: NETCONF
  - pre-6.5: CLI is a work in progress

- ALU SROS
- Juniper Junos

User Documentation
------------------

Global Configuration
~~~~~~~~~~~~~~~~~~~~

The global configuration contains the *software-pack* definitions. A
*software-pack* is essentially a profile, containing a complete description of
the desired software to be installed. This includes the *base* software image or
files and optional *patches*. Configuration is placed in the
``/software-install/software-pack`` list. These two sections are later referred
to as *components*. Each *software-pack* has a unique name (freeform string),
which is later used to associate the profile with a device.

Each *component* of a *software-pack* has two mandatory parameters - version and
filename(s) (this is a leaf-list). The version must match the version string
presented by the device, after the component is successfully installed. For
example SROS version 19.10.R5 is shown on the device C-19.10.R5. This may be
improved in the future.

For example, this is a *software-pack* definition for Cisco IOS XR, consisting
of the base image and SMUs::

  <software-install xmlns="http://terastrm.net/ns/yang/software-install.yang">
    <software-pack>
      <name>ios-6.5.2</name>
      <base>
        <version>6.5.2</version>
        <filename>/xrv9k-fullk9-x-6.5.2.iso</filename>
      </base>
      <patch>
        <version>xrv9k-6.5.2.CSCvo03672</version>
        <filename>/xrv9k-6.5.2.CSCvo03672.tar</filename>
      </patch>
      <os>ios-xr</os>
    </software-pack>
  </software-install>


Via NCS CLI for Nokia::

  set software-install software-pack alu-19.10.R5 os sros base version 19.10.R5 filename [ /ncs/software-install/alu-19.10.R5/boot.ldr /ncs/software-install/alu-19.10.R5/cpm.tim /ncs/software-install/alu-19.10.R5/iom.tim /ncs/software-install/alu-19.10.R5/support.tim ]


Other global configuration provides knobs for various aspects of the
installation process and background workers. The full model is shown here::

  module: software-install
    +--rw software-install
      +--rw enabled?                      boolean
      +--rw confirm-steps?                boolean
      +--rw auto-execute-after-confirm?   boolean
      +--rw error-handling
         +--rw max-retries?   uint32
         +--rw backoff
            +--rw (backoff-strategy)?
               +--:(factor)
               |  +--rw factor?     decimal64
               +--:(constant)
                  +--rw constant?   uint32

The *confirm-steps* leaf is a global setting for requiring confirmation of
individual steps of the installation process. The default behavior requires the
user to explicitly execute the request after confirming steps. This can be
changed to automatic execution if the *auto-execute-after-confirm* leaf is set.
The *error-handling* container controls the behavior of the background workers
in case of errors.

Device Configuration
~~~~~~~~~~~~~~~~~~~~

After configuring a global profile, it can be applied on a device::

  mzagozen@bn831x2a-oss2-lab-nso> show configuration devices device 901-R1-2050 software-pack | display set
  set devices device 901-R1-2050 software-pack name alu-7750-19.10.R3

This configuration does not start the installation process; it merely associates
a device with a global profile. If an installation is currently in progress for
the device, it is not interrupted, nor are the current parameters changed.

Creating a Request
~~~~~~~~~~~~~~~~~~

The install process is only started by running a *request*. In a nutshell, the
request contains everything the worker needs to perform the installation. This
includes a copy of the *software-profile* data. Conversely, this means that once
a software install request is created, the profile data is immutable, for that
particular request. Of course it is possible to make changes to the global
configuration, but a new request must be created for devices.

To create a new request, use the
``/devices/device{dut}/software-pack/create-request`` action. The action is
idempotent - if the input data (global *software-pack*) has not changed, it will
not create a new request. If it has changed, a new request is created and the
existing requests are marked as obsolete::

  admin@ncs> request devices device 901-R1-2050 software-pack create-request
  request-id 1
  status new-request

  admin@ncs> request devices device 901-R1-2050 software-pack create-request
  request-id 1
  status existing-request

The requests (current and past) are available as operational data on a device.
The current (latest) request has the *obsolete*  leaf set to false::

  mzagozen@bn831x2a-oss2-lab-nso> show devices device 901-R1-2050 software-pack
  software-pack request 1
    status        unprocessed
    confirm-steps true
    software-pack-data name alu-7750-19.10.R5
    software-pack-data base version C-19.10.R5
    software-pack-data base filename [ /ncs/software-install/alu-7750-19.10.R5/boot.ldr /ncs/software-install/alu-7750-19.10.R5/cpm.tim /ncs/software-install/alu-7750-19.10.R5/iom.tim /ncs/software-install/alu-7750-19.10.R5/support.tim ]
    software-pack-data os sros
    error-count transient 0
    error-count other 0
    obsolete      false

Executing (Running) a Request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An existing request is scheduled to run using the ``execute-request`` action on
the request path. Requests are executed asynchronously::

  admin@ncs> request devices device alu software-pack request 1 execute-request
  status enqueued

When the request is first executed, the execution plan is created and stored as
part of the request. The plan consists of *components*, which consist of
*steps*. Components are a 1:1 mapping from components in a profile. Steps depend
on the platform and component type. The example below shows a single (base)
component with a number of steps (on ALU)::

  admin@ncs> show devices device 901-R1-2050 software-pack request 1 component  | tab | de-select step error
                                                                            BY
  NAME             COMPLETED  NAME                       STATUS       WHEN  USER  WHEN
  --------------------------------------------------------------------------------------
  base-C-19.10.R3  false      CheckFiles                 reached      -     -     -
                              CheckVersions              failed       -     -     -
                              ActivatePrimary            not-reached  -     -     -
                              GetBootTime                not-reached  -     -     -
                              CopyImage                  not-reached  -     -     -
                              PrepareCopyBootLdr         not-reached  -     -     -
                              PrepareConfigureBof        not-reached  -     -     -
                              PrepareHackFormatStandby   not-reached  -     -     -
                              PrepareSaveRollback        not-reached  -     -     -
                              PrepareSynchronizeBootenv  not-reached  -     -     -
                              Reboot                     not-reached  -     -     -
                              Done                       not-reached  -     -     -

An execution of a request is also referred to as a *run*. The plan shown above
lists all possible steps that may be executed in a run. Some steps may be
skipped in a *run* because they have already been completed or are no longer
relevant. While the request is running, observe the run log for messages
specific to that run::

  admin@ncs> show devices device 901-R1-2050 software-pack request 1 run-log run-id 2 | tab
                                    RUN
  WHEN                              ID   COMPONENT        STEP           MESSAGE

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  2020-05-08T17:55:14.840101+00:00  2    base-C-19.10.R3  CheckFiles     Executing CheckFiles.execute

  2020-05-08T17:55:14.915642+00:00  2    base-C-19.10.R3  CheckFiles     Execution of CheckFiles.execute result StepResult.SUCCESS

  2020-05-08T17:55:14.915842+00:00  2    base-C-19.10.R3  CheckFiles     CheckFiles.execute result: StepResult.SUCCESS

  2020-05-08T17:55:15.013863+00:00  2    base-C-19.10.R3  CheckVersions  Executing CheckVersions.execute

  2020-05-08T17:55:16.161601+00:00  2    base-C-19.10.R3  CheckVersions  Error executing CheckVersions.execute - Error: Unknown error (56): Failed to connect to device 901-R1-2050: connection refused: Connection refused (Connection refused) in new state
  2020-05-08T17:55:16.26578+00:00   2    base-C-19.10.R3  -              /ncs:devices/device{901-R1-2050}/sw-install:software-pack/request{1}: result: failed-other

To find out the current (latest) *run-id* of the executing request read the
value of the *run-id-count* leaf in the request list entry::

  admin@ncs> show devices device 901-R1-2050 software-pack request 1 run-id-count
  run-id-count 2

Throughout the lifetime of a request, the run log can grow considerably. There
is currently no facility to remove old run logs. A global action on a request
level is available to remove *all* log messages::

  admin@ncs> request devices device 901-R1-2050 software-pack request 1 clear-run-log
  success true

Confirming a Request (Step)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The software-install package may be configured to require confirmation of steps,
before proceeding. If the global or per-request ``confirm-steps`` setting is
enabled, each step must be confirmed with an action once in the lifetime of a
request. The ``confirm-steps`` action can be used to confirm individual steps,
or all steps in a request. If the next step to execute does require
confirmation, the request run will stop and the request will be put into the
``waiting-confirmation`` status. The steps requiring confirmation will also
reflect this setting::

  admin@ncs> show devices device 901-R1-2050 software-pack request 1 component | tab
                                                                                            BY
  NAME             COMPLETED  NAME                       STATUS                ERROR  WHEN  USER  WHEN
  ------------------------------------------------------------------------------------------------------
  base-C-19.10.R3  false      CheckFiles                 waiting-confirmation  -      -     -     -
                              CheckVersions              waiting-confirmation  -      -     -     -
                              ActivatePrimary            waiting-confirmation  -      -     -     -
                              GetBootTime                waiting-confirmation  -      -     -     -
                              CopyImage                  waiting-confirmation  -      -     -     -
                              PrepareCopyBootLdr         waiting-confirmation  -      -     -     -
                              PrepareConfigureBof        waiting-confirmation  -      -     -     -
                              PrepareHackFormatStandby   waiting-confirmation  -      -     -     -
                              PrepareSaveRollback        waiting-confirmation  -      -     -     -
                              PrepareSynchronizeBootenv  waiting-confirmation  -      -     -     -
                              Reboot                     waiting-confirmation  -      -     -     -
                              Done                       waiting-confirmation  -      -     -     -

Use the ``confirm-steps`` action to confirm single steps or all steps::

  admin@ncs> request devices device 901-R1-2050 software-pack request 1 confirm-step component { name base-C-19.10.R3 step [ CheckFiles CheckVersions ] }
  admin@ncs> request devices device 901-R1-2050 software-pack request 1 confirm-step all
