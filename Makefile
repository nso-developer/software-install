include nidvars.mk

# The default testenv should be a "quick" testenv, containing a simple test that
# is quick to run.
DEFAULT_TESTENV?=full-vmx

# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk

# The rest of this file is specific to this repository.

push-testnso:
	docker push $(IMAGE_PATH)$(PROJECT_NAME)/testnso:$(DOCKER_TAG)

pull-package:
	docker pull $(IMAGE_PATH)$(PROJECT_NAME)/package:$(DOCKER_TAG)

generate-test-jobs:
	nid/generate-test-jobs > test-jobs.yaml
