import pathlib
from typing import Iterable, Optional, Tuple, Type

from .context import (Context, SoftwarePackComponentBase,
                      SoftwarePackComponentPatch, State, StateIosXr)
from .software_install_script import CiscoIosXrOperations, get_size
from .step_executor_classes import (StepExecutor, StepExecutorSupportsPreCheck,
                                    StepResult)


def get_steps(_state: StateIosXr) -> Iterable[Type[StepExecutor]]:
    return (ValidatePlatform,
            CheckFiles,
            CheckVersions,
            GetBootTime,
            CopyImage,
            SoftwareAdd,
            InstallPrepareClean,
            SoftwarePrepare,
            SoftwareActivate,
            SoftwareCommit,
            Cleanup,
            Done)


def _get_device_ops(context: Context, state: StateIosXr) -> CiscoIosXrOperations:
    return CiscoIosXrOperations(context, state.device, vr=state.virtual_router)


class ValidatePlatform(StepExecutor):
    def execute(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)
        result =o.validate_platform()
        if result:
            self.context.log.error(result)
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS


class CheckFiles(StepExecutor):
    def execute(self, state: State) -> StepResult:
        for f in state.software_pack_component.filenames:
            if not pathlib.Path(f).is_file():
                self._add_oper_log(f'File {f} does not exist!')
                return StepResult.FAILURE
        return StepResult.SUCCESS


class CheckVersions(StepExecutor):
    def execute(self, state: StateIosXr) -> StepResult:
        if not state.packages:
            state.packages = []
            for f in state.software_pack_component.filenames:
                if f.endswith('.iso'):
                    state.packages.append(CiscoIosXrOperations.get_version_from_iso(f))
                else:
                    state.packages.extend(CiscoIosXrOperations.get_file_packages(f))
        return StepResult.SUCCESS

    def next_step(self, state: StateIosXr) -> Optional[Type[StepExecutor]]:
        o = _get_device_ops(self.context, state)
        base_version = o.get_active_version()
        if base_version is None:
            raise Exception('Could not read currently active base OS version')
        state.virtual_router = 'xrv' in base_version

        if isinstance(state.software_pack_component, SoftwarePackComponentPatch):
            missing_active = []
            missing_committed = []
            for p in state.packages:
                if not o.get_active_version(p):
                    missing_active.append(p)
                    continue
                if not o.get_committed_version(p):
                    missing_committed.append(p)
            if missing_active:
                self.context.log.debug(f'Found missing active packages: {missing_active}')
                return GetBootTime
            elif missing_committed:
                # all packages are active, but some are not committed
                self.context.log.debug(f'Found missing committed packages: {missing_committed}')
                return SoftwareCommit
            else:
                return Cleanup
        elif isinstance(state.software_pack_component, SoftwarePackComponentBase):
            if base_version != state.packages[0]:
                self.context.log.debug(f'Active base version {base_version} differs from target {state.packages[0]}')
                return GetBootTime
            committed_version = o.get_committed_version()
            if committed_version != state.packages[0]:
                self.context.log.debug(f'Committed base version {committed_version} differs from target {state.packages[0]}')
                return SoftwareCommit
            return Cleanup
        else:
            return None

class CopyImage(StepExecutor, StepExecutorSupportsPreCheck):
    def _is_file_present(self, state: StateIosXr, local_file: str) -> Tuple[bool, Optional[str]]:
        o = _get_device_ops(self.context, state)
        expected_size = get_size(local_file)
        assert(state.destination_volume is not None)
        files = o.list_files(state.destination_volume)
        filename = pathlib.Path(local_file).name
        try:
            size = files[filename]
            if size == expected_size:
                return True, None
            else:
                return False, f'Expected size of {filename} {expected_size} does not match actual {size}'
        except KeyError:
            return False, f'File {filename} not found in {state.destination_volume}'

    def pre_check(self, state: StateIosXr) -> StepResult:
        """Check if the image already exists, or there is enough available space for the image"""

        o = _get_device_ops(self.context, state)
        if not state.destination_volume:
            state.destination_volume, free = o.get_free_space()
        else:
            _, free = o.get_free_space()

        missing = set(state.software_pack_component.filenames)
        errors = []
        for f in state.software_pack_component.filenames:
            ok, error = self._is_file_present(state, f)
            if ok:
                missing.remove(f)
                if f not in state.destination_paths:
                    state.destination_paths[f] = str(pathlib.Path(state.destination_volume) / pathlib.Path(f).name)
            else:
                state.destination_paths.pop(f, None)
                errors.append(error)
        if errors:
            self.context.log.debug(errors)
        else:
            return StepResult.SKIP_STEP

        total_size = sum(get_size(f) for f in state.software_pack_component.filenames if f in missing)
        # The file we're copying must fit into the destination partition. It
        # will be extracted elsewhere, so there is no need for a buffer.
        if total_size > free:
            self._add_oper_log(f'Not enough available space: image={total_size}, free={free}')
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateIosXr) -> StepResult:
        """Copy the image to destination"""
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)

        any_errors = False
        for f in state.software_pack_component.filenames:
            if f in state.destination_paths:
                continue

            state.destination_paths[f] = o.copy_image(f, state.destination_volume)

            present, error = self._is_file_present(state, f)
            if not present:
                assert(error is not None)
                self._add_oper_log(error)
                any_errors = True
        if any_errors:
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS


class SoftwareAdd(StepExecutor, StepExecutorSupportsPreCheck):
    def pre_check(self, state: StateIosXr) -> StepResult:
        if state.op_id_add:
            o = _get_device_ops(self.context, state)
            done, success, _op_log, _error = o.fetch_install_operation_log(state.op_id_add)
            if done and success:
                return StepResult.SKIP_STEP
            elif not done:
                return StepResult.WAIT
        return StepResult.SUCCESS

    def execute(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)
        result = o.prepare_add(state.destination_paths.values())
        if result.op_id:
            state.op_id_add = result.op_id
        if result.result:
            if isinstance(state.software_pack_component, SoftwarePackComponentPatch):
                # 'install add' log contains information on the packages added (for patches)
                # if result.result is True, op_log will be set
                assert(result.op_log is not None)
                packages = list(CiscoIosXrOperations.extract_package_info_from_log(result.op_log))
                state.packages = packages
            return StepResult.SUCCESS
        else:
            self._add_oper_log(f"Error executing 'install add {state.destination_paths.values()}': {result.error}\n{result.op_log}")
            return StepResult.FAILURE


class InstallPrepareClean(StepExecutor):
    def execute(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)
        result = o.prepare_clean()
        if result.result:
            state.restart_prepare_clean()
            return StepResult.SUCCESS
        else:
            self._add_oper_log(f"Error executing 'install prepare clean': {result.error}\n{result.op_log}")
            return StepResult.FAILURE


class SoftwarePrepare(StepExecutor, StepExecutorSupportsPreCheck):
    def pre_check(self, state: StateIosXr) -> StepResult:
        if state.op_id_prepare:
            o = _get_device_ops(self.context, state)
            done, success, _op_log, _error = o.fetch_install_operation_log(state.op_id_prepare)
            if done and success:
                return StepResult.SKIP_STEP
            elif not done:
                return StepResult.WAIT
        return StepResult.SUCCESS

    def execute(self, state: StateIosXr) -> StepResult:
        state.restart_prepare()
        o = _get_device_ops(self.context, state)
        assert(state.op_id_add is not None)
        result = o.prepare_prepare(state.op_id_add)
        if result.op_id:
            state.op_id_prepare = result.op_id
        if result.result:
            return StepResult.SUCCESS
        else:
            self._add_oper_log(f"Error executing 'install prepare {state.op_id_add}': {result.error}\n{result.op_log}")
            return StepResult.FAILURE


class SoftwareActivate(StepExecutor, StepExecutorSupportsPreCheck):
    def pre_check(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)

        if state.op_id_activate:
            done, success, op_log, _error = o.fetch_install_operation_log(state.op_id_activate)
            if op_log:
                state.reload_required = o.activate_requires_reload(op_log)

            if done and success:
                return StepResult.SKIP_STEP
            elif not done:
                return StepResult.WAIT

        missing = []
        for p in state.packages:
            if o.get_active_version(p) is None:
                missing.append(p)
        if not missing:
            # all packages have been activated!
            return StepResult.SKIP_STEP
        return StepResult.SUCCESS

    def execute(self, state: StateIosXr) -> StepResult:
        state.restart_activate()
        o = _get_device_ops(self.context, state)
        result = o.activate()
        state.op_id_activate = result.op_id
        if result.op_log:
            state.reload_required = o.activate_requires_reload(result.op_log)
        if result.result:
            return StepResult.SUCCESS
        else:
            self._add_oper_log(f"Error executing 'install activate': {result.error}\n{result.op_log}")
            return StepResult.FAILURE


class GetBootTime(StepExecutor):
    def execute(self, state: StateIosXr) -> StepResult:
        if state.boot_time is None:
            o = _get_device_ops(self.context, state)
            state.boot_time = o.get_boot_time()
        return StepResult.SUCCESS


class SoftwareCommit(StepExecutor, StepExecutorSupportsPreCheck):
    def pre_check(self, state: StateIosXr) -> StepResult:
        # Before proceeding with "commit", check:
        #  1. if required, device was rebooted (uptime has changed)
        #  2. all packages were activated

        o = _get_device_ops(self.context, state)

        if state.reload_required and state.boot_time is not None:
            boot_time = o.get_boot_time()
            if boot_time <= state.boot_time:
                # not restarted yet, fail the step
                self.context.log.warning(f'Current boot-time {boot_time} <= {state.boot_time}')
                return StepResult.WAIT

        current_request = o.get_current_install_request()
        # if current_request := o.get_current_install_request() in Python 3.8 <3
        if current_request is not None:
            self._add_oper_log(f'Install request currently in progress: {current_request}')
            return StepResult.WAIT

        if state.op_id_commit:
            done, success, _op_log, _error = o.fetch_install_operation_log(state.op_id_commit)
            if done and success:
                return StepResult.SKIP_STEP
            elif not done:
                return StepResult.WAIT

        missing = []
        for p in state.packages:
            if o.get_active_version(p) is None:
                missing.append(p)
        if missing:
            self._add_oper_log(f'Packages {",".join(missing)} still not activated')
            return StepResult.WAIT
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)
        result = o.commit()
        if result.op_id:
            state.op_id_commit = result.op_id
        if result.result:
            return StepResult.SUCCESS
        else:
            self._add_oper_log(f"Error executing 'install commit': {result.error}\n{result.op_log}")
            return StepResult.FAILURE


class Cleanup(StepExecutor):
    def execute(self, state: StateIosXr) -> StepResult:
        o = _get_device_ops(self.context, state)
        for f in state.destination_paths.values():
            o.delete_file(f)
        return StepResult.SUCCESS


class Done(StepExecutor):
    def execute(self, state: State) -> StepResult:
        state.done = True
        return StepResult.SUCCESS
