from __future__ import annotations

import contextlib
import logging
import logging.handlers
import queue
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import Any, Dict, Iterable, Iterator, List, Optional, Type, cast

import jsonpickle

import _ncs
import ncs

from . import format_yang_date_and_time


class EnrichedLogRecord(logging.LogRecord):
    swi_request_path: Optional[str]
    swi_component: Optional[str]
    swi_step: Optional[str]
    swi_run_id: Optional[int]


class OperCdbLoggingHandler(logging.Handler):
    """A logger sink for NSO - operational data and CLI messages

    Log messages should be visible to the user as operational data. The logging
    library already provides a natural logging API so it makes sense to keep the
    interface consistent across different methods of logging. This logging
    handler will receive enriched log records with additional information on the
    software-pack component and step. If a record is of INFO level or higher (we
    skip debug), it is emitted to the appropriate NSO CDB path.

    The actual logging is done in a separate thread with the QueueHandler /
    QueueListener pattern.
    """
    _maapi: ncs.maapi.Maapi
    _queue_handler: logging.handlers.QueueHandler
    _listener: logging.handlers.QueueListener

    def __init__(self, logger: logging.Logger, level=logging.NOTSET, user_session_id: Optional[int] = None):
        super().__init__(level)
        self._logger = logger
        self._user_session_id = user_session_id

    def emit(self, record: logging.LogRecord):
        enriched_record = cast(EnrichedLogRecord, record)
        if enriched_record.levelno > logging.DEBUG:
            with self._maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                node = ncs.maagic.get_node(oper_t_write, enriched_record.swi_request_path).run_log
                created = datetime.fromtimestamp(enriched_record.created)
                entry = node.create(format_yang_date_and_time(created))
                entry.run_id = enriched_record.swi_run_id
                entry.step = enriched_record.swi_step
                entry.component = enriched_record.swi_component
                entry.message = enriched_record.getMessage()
                oper_t_write.apply()
        if self._user_session_id:
            # We can print the message to the CLI session when an action is
            # executing - callback is active and blocking. If the action is not
            # running, user session is no longer "writable".
            if enriched_record.swi_step:
                msg = f'{enriched_record.swi_request_path} ({enriched_record.swi_run_id}): {enriched_record.swi_component} {enriched_record.swi_step}: {enriched_record.getMessage()}'
            else:
                msg = f'{enriched_record.swi_request_path} ({enriched_record.swi_run_id}): {enriched_record.swi_component} {enriched_record.getMessage()}'
            try:
                self._maapi.cli_write(self._user_session_id, msg + '\n')
            except _ncs.error.Error as e:
                # Forget about printing to CLI in the future if the session is
                # no longer valid.
                if 'no cli session' in str(e):
                    self._user_session_id = None
                else:
                    raise

    @staticmethod
    def _log_filter(record: logging.LogRecord) -> bool:
        """Simple logging Filter to reject messages with no context info"""
        return hasattr(record, 'swi_component')

    def __enter__(self):
        self._maapi = ncs.maapi.Maapi()
        self._maapi.start_user_session('python-software-install-logger', 'system')

        q: queue.Queue = queue.Queue()
        self._queue_handler = logging.handlers.QueueHandler(q)
        self._queue_handler.addFilter(self._log_filter)
        self._listener = logging.handlers.QueueListener(q, self)
        self._listener.start()
        self._logger.addHandler(self._queue_handler)

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self._listener.stop()
        self._logger.removeHandler(self)
        self._maapi.end_user_session()
        self._maapi.close()


@dataclass
class Context:
    maapi: ncs.maapi.Maapi
    log: logging.Logger
    request_path: Optional[str] = None
    component: Optional[str] = None
    step: Optional[str] = None
    run_id: Optional[int] = None

    @contextlib.contextmanager
    def enter_step(self, step: str):
        try:
            self.step = step
            yield
        finally:
            self.step = None

    def add_oper_log(self, message: str, step: Optional[str] = None):   # pylint: disable=unused-argument
        self.log.error(message)

    def _log_filter(self, record: logging.LogRecord) -> bool:
        """Filter callback that "enriches" the LogRecord with additional attributes

        The additional attributes swi_component, swi_step and are used in the
        custom OperCdbLoggingHandler to place the log message on the correct
        path.

        Note: filters "do not propagate" in the logger hierarchy. A filter
        object (or callable) will only be used for the logger where the filter
        is attached. This means that messages from other loggers (other modules
        for example, like paramiko) will _not_ be enriched with additional
        context information.
        """
        enriched_record = cast(EnrichedLogRecord, record)
        enriched_record.swi_request_path = self.request_path
        enriched_record.swi_component = self.component
        enriched_record.swi_step = self.step
        enriched_record.swi_run_id = self.run_id
        return True

    def __enter__(self):
        self.log.addFilter(self._log_filter)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.log.removeFilter(self._log_filter)


class SoftwarePackOs(Enum):
    IOSXR = 'ios-xr'
    VRP = 'vrp'
    JUNOS = 'junos'
    SROS = 'sros'


@dataclass(unsafe_hash=True)
class SoftwarePackComponent:
    # We use a frozen dataclass here for two reasons:
    # 1. the object is hashable, so it can be used as a dict key,
    # 2. two objects with the same attribute values are equal.
    version: str
    filenames: Iterable[str]
    os: SoftwarePackOs

    @property
    def name(self) -> str:
        if isinstance(self, SoftwarePackComponentBase):
            return f'base-{self.version}'
        else:
            return f'patch-{self.version}'

    @classmethod
    def from_cdb(cls, data: ncs.maagic.Container, os: SoftwarePackOs) :
        return cls(data.version, tuple(data.filename.as_list()), os)


class SoftwarePackComponentBase(SoftwarePackComponent):
    pass


class SoftwarePackComponentPatch(SoftwarePackComponent):
    pass


@dataclass
class SoftwarePack:
    name: str
    os: SoftwarePackOs
    base: Optional[SoftwarePackComponentBase] = None
    patch: List[SoftwarePackComponentPatch] = field(default_factory=list)

    @staticmethod
    def from_cdb(data: ncs.maagic.Container) -> SoftwarePack:
        sp = SoftwarePack(data.name, SoftwarePackOs(data.os.string))

        if data.base.exists():
            sp.base = SoftwarePackComponentBase.from_cdb(data.base, sp.os)

        for p in data.patch:
            sp.patch.append(SoftwarePackComponentPatch.from_cdb(p, sp.os))

        return sp


    def to_cdb(self, data: ncs.maagic.Container):
        data.name = self.name
        data.os = self.os.value
        if self.base:
            base = data.base.create()
            base.version = self.base.version
            base.filename = self.base.filenames
        else:
            del data.base

        if self.patch:
            for p in self.patch:
                patch = data.patch.create(p.version)
                patch.filename = p.filenames
        else:
            del data.patch

    def components(self) -> Iterator[SoftwarePackComponent]:
        if self.base:
            yield self.base
        yield from self.patch


@dataclass
class State:
    """The State object is used to pass calculated data between software upgrade steps.

    In code, the object is created with the 'from_component()' static method.
    After an attribute is changed, use the 'flush()' method to persist the data
    to CDB. The object is serialized to JSON. Example:

        request = ncs.maagic.get_node(t_read, f'/devices/device{{{device}}}/software-pack/request{{{request_id}}}')
        component = SoftwarePackComponentBase('1.2.3', 'file.bin', SoftwarePackOs.IOSXR)
        state = State.from_component(request, component)
        # set an attribute
        state.destination_filesystem = 'disk0:'
        state.flush(oper_t_write)
    """
    device: str
    software_pack_component: SoftwarePackComponent
    # store the keypath to component in the device model, so that we're able to
    # flush the state
    _spc_path: Optional[str] = field(default=None, init=False, repr=False, compare=False)
    # some steps behave differently for a virtual router (CI)
    virtual_router: bool = False
    done: bool = False

    @staticmethod
    def from_component(request: ncs.maagic.Node, component: SoftwarePackComponent) -> 'State':
        try:
            internal_state = request.component[component.name].internal_state
            return jsonpickle.decode(internal_state)
        except (KeyError, TypeError):
            # the plan component doesn't exist or internal_state is None
            state_cls: Type[State]
            if component.os == SoftwarePackOs.IOSXR:
                state_cls = StateIosXr
            elif component.os == SoftwarePackOs.JUNOS:
                state_cls = StateJunos
            elif component.os == SoftwarePackOs.SROS:
                state_cls = StateSros
            else:
                state_cls = State
            device = request._parent._parent._parent
            state = state_cls(device.name, component)
            state._spc_path = f'{request._path}/component{{{component.name}}}'
            return state

    def flush(self, oper_t_write: ncs.maapi.Transaction):
        spc = ncs.maagic.get_node(oper_t_write, self._spc_path)
        spc.internal_state = jsonpickle.encode(self)

    def reset(self):
        self.virtual_router = False


@dataclass
class GenericDevice:
    """Intermediate State object for storing generic device attributes

    The attributes defined in this class apply to a single target "device"
    where software is installed. If the target device chassis has redundant
    control planes (like Juniper REs), these attributes are control plane
    specific."""
    # after checking free space, store the volume name with most space available
    destination_volume: Optional[str] = None
    # after copying the image, store the full paths to the image
    destination_paths: Dict[str, str] = field(default_factory=dict)
    # detect device reboot after package activation by checking boot time
    boot_time: Optional[datetime] = None
    done: bool = False

    def reset(self) -> None:
        """Reset the state object to "initial" state

        Sets all attributes to their initial values. This is kind of like
        creating a new instance, except we keep the existing object. Not sure if
        this is really a good idea?? Maybe I should change the step executor
        methods to just return the state instead?
        """
        self.destination_volume = None
        self.destination_paths = {}
        self.boot_time = None


@dataclass
class StateIosXr(GenericDevice, State):
    """Specialized State object for IOS XR"""
    # The ID of the 'install add' operation. This is needed for executing
    # 'install prepare $op_id'
    op_id_add: Optional[int] = None
    op_id_prepare: Optional[int] = None
    op_id_activate: Optional[int] = None
    op_id_commit: Optional[int] = None
    # Contents of a software pack component (list of packages+versions)
    packages: List[str] = field(default_factory=list)
    reload_required: bool = False

    def restart_prepare_clean(self):
        self.op_id_prepare = None
        self.restart_activate()

    def restart_prepare(self):
        self.op_id_activate = None
        self.restart_activate()

    def restart_activate(self):
        self.op_id_commit = None

    def reset(self):
        super().reset()
        self.op_id_add = None
        self.op_id_prepare = None
        self.op_id_activate = None
        self.op_id_commit = None
        self.packages = []
        self.reload_required = False


@dataclass
class RouteEngine(GenericDevice):
    """Specialized GenericDevice class for Junos"""
    version: Optional[str] = None
    # move this to base class ...
    rebooted: bool = False


@dataclass
class StateJunos(State):
    """"Specialized State object for Junos"""

    dual_re: Optional[bool] = None
    switch: bool = False
    failover_config: Any = None
    route_engine: Dict[str, RouteEngine] = field(default_factory=dict)
    # the priority list contains re_ids starting with master
    route_engine_priority: List[int] = field(default_factory=list)

    def reset(self) -> None:
        self.failover_config = None
        for re_id in self.route_engine.keys():
            self.route_engine[re_id] = RouteEngine()


@dataclass
class StateSros(GenericDevice, State):
    """"Specialized State object for SROS"""
    version: Optional[str] = None
    # move this to base class ...
    rebooted: bool = False
