import enum

import ncs


class DeviceOs(enum.Enum):
    SROS_CLI = "SROS_CLI"
    SROS_NC = "SROS_NC"
    IOSXR = "IOSXR"
    JUNOS = "JUNOS"
    VRP = "VRP"
    SNABB_v1 = "SNABB-v1"
    SNABB_v2 = "SNABB-v2"
    #OPLINK = "OPLINK"
    ONS_TL1 = "ONS-TL1"
    HGW = "HGW"


def get_dev_os(ncs_device: ncs.maagic.Node) -> DeviceOs:
    """Figure out the device OS based on the advertised capabilities

    :param device: the /devices/device list entry
    :return: detected DeviceOs enum
    """

    os = None
    for cap in ncs_device.capability:
        if cap.uri == "http://tail-f.com/ned/alu-sr":
            os = DeviceOs.SROS_CLI
        if cap.uri == "urn:nokia.com:sros:ns:yang:sr:conf":
            os = DeviceOs.SROS_NC
        if cap.uri == "http://cisco.com/ns/yang/cisco-xr-types":
            os = DeviceOs.IOSXR
        if cap.uri == "urn:juniper-rpc":
            os = DeviceOs.JUNOS
        if cap.uri == "http://tail-f.com/ned/huawei-vrp":
            os = DeviceOs.VRP
        if cap.uri == "snabb:softwire-v2":
            os = DeviceOs.SNABB_v2
        if cap.uri == "snabb:lwaftr" and os is None:
            os = DeviceOs.SNABB_v1
        #if cap.uri == "http://com/oplink/device":
        #   os = DeviceOs.OPLINK
        if cap.uri == "http://terastream.net/ned/ons-tl1":
            os = DeviceOs.ONS_TL1
        if cap.uri in ("http://terastrm.net/ns/yang/terastream-software", "http://terastrm.net/ns/yang/terastream-software-hack"):
            os = DeviceOs.HGW

    if os is None:
        if len(ncs_device.capability):
            capabilities = ','.join(cap.uri for cap in ncs_device.capability)
            raise ValueError('Unknown device OS (capabilities: {})'.format(capabilities))
        else:
            raise ValueError('Device has no capabilities')

    return os
