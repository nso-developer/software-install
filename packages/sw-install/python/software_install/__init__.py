from datetime import datetime
from typing import Optional


def format_yang_date_and_time(timestamp: Optional[datetime]=None, microseconds=True) -> str:
    """Format a timestamp in yang:date-and-time format

    :param timestamp: optional datetime.datetime instance. Defaults to utcnow()
    :param microseconds: whether to include microseconds in the timestamp. Defaults to True
    :return: yang:date-and-time formatted string"""
    if not timestamp:
        timestamp = datetime.utcnow()
    elif not isinstance(timestamp, datetime):
        raise ValueError('timestamp must be datetime.datetime instance')
    if microseconds:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
    else:
        fmt = '%Y-%m-%dT%H:%M:%SZ'
    return timestamp.strftime(fmt)
