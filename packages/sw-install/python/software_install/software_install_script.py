"""
The upgrade process can be roughly divided into three stages:
- prepare:
    - determine the destination location on the device (multiple filesystems?)
    - copy the software to the device
    - validate the software package (checksum or command)
    - any other actions that we can perform that won't affect the running software, even after a reboot
- activate
    - activate the prepared software
    - reboot if needed
- commit
    - persist the setting for boot image
    - remove the old software from filesystem
"""


import base64
import contextlib
import dataclasses
import hashlib
import io
import logging
import pathlib
import re
import stat
import tarfile
import time
import timeit
from datetime import datetime, timedelta
from typing import Dict, Iterable, Iterator, List, Optional, Tuple, Union
from typing_extensions import Protocol

import humanize
import paramiko
import pycdlib
import yaml
import ncclient
import ncclient.manager

import _ncs
import ncs

from . import device_os


class CommunicationError(Exception):
    pass


def _decrypt_encrypted_string(node, value):
    """Decrypts an encrypted tailf:aes-cfb-128-encrypted-string type leaf

    :param node: any maagic Node
    :param value: the encrypted leaf value
    """
    if value is None:
        return None

    if isinstance(node._backend, ncs.maagic._TransactionBackend):
        node._backend.maapi.install_crypto_keys()
    elif isinstance(node._backend, ncs.maagic._MaapiBackend):
        node._backend.install_crypto_keys()
    else:
        raise ValueError("Unknown MaagicBackend for leaf")

    return _ncs.decrypt(value) #pylint: disable=no-member


@dataclasses.dataclass(frozen=True)
class DeviceCredentials:
    address: str
    port: int
    scp_port: int
    username: str
    password: Optional[str] = None
    rsa_private_key: Optional[str] = None
    rsa_public_key: Optional[str] = None


    @staticmethod
    def from_cdb(context, device: str) -> Optional['DeviceCredentials']:
        """Read the currently configured device credentials for the given device by looking at the authgroup

        If the device-specific authgroup exists, the returned credentials are decrypted and the private
        RSA key, if present, prepared in the expected format.

        :param root: maagic Root
        :param device: device name
        :returns: populated DeviceCredentials object if authgroup exists, or None"""

        with context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            authgroup_name = root.devices.device[device].authgroup
            try:
                authgroup = root.devices.authgroups.group[authgroup_name].default_map
            except KeyError:
                return None
            username = authgroup.remote_name
            encrypted_password = authgroup.remote_password
            if encrypted_password is not None:
                password = _decrypt_encrypted_string(root, encrypted_password)
            else:
                password = None
            address = root.devices.device[device].address
            try:
                port = int(root.devices.device[device].port)
            except TypeError:
                port = 830
            try:
                scp_port = int(root.devices.device[device].scp_port)
            except TypeError:
                scp_port = 22

            private_key_name = authgroup.public_key.private_key.name
            if private_key_name is not None:
                encrypted_private_key = root.ssh.private_key[private_key_name].key_data
                private_key = _decrypt_encrypted_string(root, encrypted_private_key)
                # The private key stored in /ssh/private-key/key-data is stored base64-encoded, with an
                # additional 20 byte header. To restore it to the original input, we do the following:
                #   1. base64 decode the decrypted value
                #   2. strip away the first 20 bytes
                #   3. base64 encode the value and add the RSA key prefix and suffix

                private_key_bytes = private_key.encode()
                private_key_stripped = base64.b64decode(private_key_bytes)[20:]
                private_key = base64.b64encode(private_key_stripped).decode()
                private_key = '-----BEGIN RSA PRIVATE KEY-----\n' + \
                            '\n'.join(private_key[i:i + 64] for i in range(0, len(private_key), 64)) + \
                            '\n-----END RSA PRIVATE KEY-----\n'
            else:
                private_key = None

            dc = DeviceCredentials(address, port, scp_port, username, password=password, rsa_private_key=private_key)
            return dc


@dataclasses.dataclass
class JunosFailoverConfig:
    graceful_switchover: bool
    failover_on_loss_of_keepalives: bool
    failover_on_disk_failure: bool

    @property
    def configured(self) -> bool:
        return self.graceful_switchover or self.failover_on_loss_of_keepalives or self.failover_on_disk_failure


def hash_file(name: str, chunk_size: int = 8192) -> str:
    h = hashlib.sha256()
    with open(name, 'rb') as f:
        for c in iter(lambda: f.read(chunk_size), b''):
            h.update(c)
    return h.hexdigest()


def get_size(file_or_dir: str) -> int:
    p = pathlib.Path(file_or_dir)
    if p.is_file():
        return p.stat().st_size
    else:
        s = 0
        for f in p.rglob('*'):
            s += f.stat().st_size
        return s


class DeviceOperations:
    def __init__(self, context, device: str, vr=False):
        self.context = context
        self.device = device
        self.vr = vr

    @contextlib.contextmanager
    def _sftp_client(self) -> Iterator[paramiko.SFTPClient]:
        credentials = DeviceCredentials.from_cdb(self.context, self.device)
        if credentials is None:
            raise Exception(f'Device credentials for {self.device} could not be retrieved')

        with paramiko.SSHClient() as ssh:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if credentials.rsa_private_key:
                private_key_temp = io.StringIO(credentials.rsa_private_key)
                private_key_ready = paramiko.RSAKey.from_private_key(private_key_temp)
                ssh.connect(credentials.address, credentials.scp_port, pkey=private_key_ready,
                            username=credentials.username, timeout=5, banner_timeout=5,
                            look_for_keys=False)
            else:
                ssh.connect(credentials.address, credentials.scp_port,
                            username=credentials.username,
                            password=credentials.password, timeout=5, banner_timeout=5,
                            look_for_keys=False)

            sftp = ssh.open_sftp()
            if sftp is None:
                raise Exception('Failed to open SFTP client')
            try:
                yield sftp
            finally:
                sftp.close()

    @staticmethod
    def _sftp_progress_callback():
        # Wrap the actual callback with a closure, calculate the transfer speed
        # as an average
        start_time = time.perf_counter()
        def actual_callback(bytes_transferred: int, bytes_total: int):
            speed = bytes_transferred / (time.perf_counter() - start_time)
            print(f'transferred {humanize.naturalsize(bytes_transferred)} / {humanize.naturalsize(bytes_total)} ({int(bytes_transferred/bytes_total*100)}%) speed {humanize.naturalsize(speed)}/s')
        return actual_callback

    def scp_to(self, local_file: str, remote_file: str):
        # The scp-to command in NSO is fucking unreliable. A lot of times
        # it gets stuck creating a file of size 0 on destination ...
        # Let's just use paramiko :D
        with self._sftp_client() as sftp:
            _attrs = sftp.put(local_file, remote_file, callback=self._sftp_progress_callback(), confirm=True)

    def list_files(self, path: str = '.') -> Dict[str, int]:
        with self._sftp_client() as sftp:
            files = {}
            for entry in sftp.listdir_iter(path):
                if entry.st_mode is None or entry.st_size is None:
                    continue
                if stat.S_ISREG(entry.st_mode):
                    files[entry.filename] = entry.st_size
            return files

    def delete_file(self, path: str):
        with self._sftp_client() as sftp:
            try:
                sftp.remove(path)
            except FileNotFoundError:
                pass

    def create_dir(self, name: str):
        with self._sftp_client() as sftp:
            sftp.mkdir(name)

    def _validate_platform(self, expected: Tuple[device_os.DeviceOs, ...]) -> Optional[str]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            try:
                os = device_os.get_dev_os(d)
            except ValueError as e:
                return str(e)
            else:
                if os in expected:
                    return None
                else:
                    return f'Detected os {os} different than expected {expected}'

    def validate_platform(self) -> Optional[str]:
        ...


class JuniperJunosOperations(DeviceOperations):
    def __init__(self, context, device: str, vr=False, switch=False):
        super().__init__(context, device, vr)
        self.switch = switch

    def validate_platform(self) -> Optional[str]:
        return self._validate_platform((device_os.DeviceOs.JUNOS,))

    def get_route_engine_information(self) -> List[Tuple[int, str, str, str]]:
        """RPC get-route-engine-information

        :return: list of (slot_id, model, mastership_state, status)
        """
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            rei = root.devices.device[self.device].rpc.rpc_get_route_engine_information.get_route_engine_information()

            routing_engines = []
            for re_item in rei.route_engine_information.route_engine:
                re_data = (int(re_item.slot), re_item.model, re_item.mastership_state, re_item.status)
                routing_engines.append(re_data)
            return routing_engines


    def get_version(self, re_id: int) -> str:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            swi = root.devices.device[self.device].rpc.rpc_get_software_information.get_software_information
            swi_input = swi.get_input()
            if not self.switch:
                swi_input.invoke_on.all_routing_engines.create()
            result = swi(swi_input)

            for re_item in result.multi_routing_engine_results.multi_routing_engine_item:
                if not self.switch and re_item.re_name != f're{re_id}':
                    continue
                return re_item.software_information.junos_version
        raise ValueError(f'Routing engine re{re_id} not found')

    def get_free_space(self, re_id: int) -> Optional[Tuple[str, int]]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            s = root.devices.device[self.device].rpc.rpc_get_system_storage.get_system_storage
            si = s.get_input()
            if not self.switch:
                si.invoke_on.all_routing_engines.create()
            result = s(si)
            # the filesystem list is keyless, need to iterate though entries to find the right one
            for re_item in result.multi_routing_engine_results.multi_routing_engine_item:
                if not self.switch and re_item.re_name != f're{re_id}':
                    continue
                for fs in re_item.system_storage_information.filesystem:
                    if (not self.switch and fs.filesystem_name == 'tmpfs' and fs.mounted_on == '/.mount/tmp') or (self.switch and fs.mounted_on == '/var/tmp'):
                        # the available_blocks leaf counts blocks of 512 bytes
                        available_bytes = fs.available_blocks * 512
                        return '/var/tmp', available_bytes
                    else:
                        continue
        return None

    def copy_image(self, name: str, destination_volume: str) -> str:
        dst = str(pathlib.Path(destination_volume) / pathlib.Path(name).name)
        self.scp_to(name, dst)
        return dst

    def file_copy(self, source: str, destination: str):
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            fc = root.devices.device[self.device].rpc.rpc_file_copy.file_copy
            fci = fc.get_input()
            fci.source = source
            fci.destination = destination
            fc(fci)

    def calculate_checksum(self, path: str) -> str:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]

            gci_input = d.rpc.rpc_get_checksum_information.get_checksum_information.get_input()
            gci_input.path = path
            gci_input.method = 'sha-256'
            r = d.rpc.rpc_get_checksum_information.get_checksum_information(gci_input)
            f = next(iter(r.checksum_information.file_checksum))
            return f.checksum

    def package_add(self, name: str, re_id: int) -> Tuple[bool, str]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            add_input = d.rpc.rpc_request_package_add.request_package_add.get_input()
            add_input.package_name = name
            add_input.validate.create()
            if not self.switch:
                if re_id == 0:
                    add_input.re0.create()
                elif re_id == 1:
                    add_input.re1.create()
                else:
                    raise ValueError(f'Invalid routing-engine ID {re_id}')
            # set 'force' to force make sure downgrades succeed too
            add_input.force.create()
            r = d.rpc.rpc_request_package_add.request_package_add(add_input)
            return r.package_result == 0, r.output

    def reboot(self, re_id: int) -> bool:
        with self.context.maapi.start_read_trans() as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            reboot_input = d.rpc.rpc_request_reboot.request_reboot.get_input()
            reboot_input.message = 'Rebooting due to software upgrade'
            if not self.switch:
                if re_id == 0:
                    pass
                elif re_id == 1:
                    reboot_input.other_routing_engine.create()
                else:
                    raise ValueError(f'Invalid routing-engine ID {re_id}')
            try:
                r = d.rpc.rpc_request_reboot.request_reboot(reboot_input)
                print(r.request_reboot_results.request_reboot_status)
            except _ncs.error.Error as e:
                # vMX responds with the XML response:
                # <request-reboot-status reboot-time>Shutdown NOW! ... </request-reboot-status>
                # The 'reboot-time' attribute trips up NSO, but reboot was successful
                if self.vr and "Invalid attribute syntax in tag ''request-reboot-status''" in str(e):
                    pass
                else:
                    raise
        return True

    def list_files(self, path: str = '.') -> Dict[str, int]:
        files: Dict[str, int] = {}
        with self.context.maapi.start_read_trans() as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            rpc_input = d.rpc.rpc_file_list.file_list.get_input()
            rpc_input.detail.create()
            rpc_input.path = path
            result = d.rpc.rpc_file_list.file_list(rpc_input)
            if result.directory_list.output:
                # invalid path
                return files

            for d in result.directory_list.directory:
                for fi in d.file_information:
                    if fi.file_directory.exists():
                        continue
                    files[fi.file_name] = fi.file_size
                break
        return files

    def get_boot_time(self, re_id: int) -> datetime:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            sui = d.rpc.rpc_get_system_uptime_information.get_system_uptime_information
            sui_input = sui.get_input()
            if not self.switch:
                sui_input.invoke_on.all_routing_engines.create()
            result = sui(sui_input)

            for re_item in result.multi_routing_engine_results.multi_routing_engine_item:
                if not self.switch and re_item.re_name != f're{re_id}':
                    continue
                # date-time 2019-06-18 18:24:09 UTC
                system_booted_time = re_item.system_uptime_information.system_booted_time.date_time
                # Let's remove the TZ info. We're interested in boot time change anyway ...
                system_booted_time = re.sub(r'\s+\w+$', '', system_booted_time)
                return datetime.fromisoformat(system_booted_time)
        raise ValueError(f'Routing engine re{re_id} not found')

    def check_failover(self) -> JunosFailoverConfig:
        with self.context.maapi.start_read_trans() as t_read:
            d = ncs.maagic.get_node(t_read, f'/devices/device{{{self.device}}}')
            cr = d.ncs__config.junos__configuration.chassis.redundancy
            return JunosFailoverConfig(
                cr.graceful_switchover.exists(),
                cr.failover.on_loss_of_keepalives.exists(),
                cr.failover.on_disk_failure.exists()
            )

    def disable_failover(self):
        with self.context.maapi.start_write_trans() as t_write:
            write_device = ncs.maagic.get_node(t_write, f'/devices/device{{{self.device}}}')
            cr = write_device.ncs__config.junos__configuration.chassis.redundancy
            # chassis redundancy graceful-switchover - presence container
            if cr.graceful_switchover.exists():
                cr.graceful_switchover.delete()
            # chassis redundancy failover - empty leaves
            if cr.failover.on_loss_of_keepalives.exists():
                cr.failover.on_loss_of_keepalives.delete()
            if cr.failover.on_disk_failure.exists():
                cr.failover.on_disk_failure.delete()
            # The /configuration/version (config) leaf is updated when the
            # version changes. This is likely to cause out-of-sync.
            flags = ncs.maapi.COMMIT_NCS_NO_OUT_OF_SYNC_CHECK | ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE
            t_write.apply(flags=flags)

    def enable_failover(self, failover_config: JunosFailoverConfig):
        with self.context.maapi.start_write_trans() as t_write:
            write_device = ncs.maagic.get_node(t_write, f'/devices/device{{{self.device}}}')
            cr = write_device.ncs__config.junos__configuration.chassis.redundancy
            if failover_config.graceful_switchover:
                cr.graceful_switchover.create()
            if failover_config.failover_on_loss_of_keepalives:
                cr.failover.on_loss_of_keepalives.create()
            if failover_config.failover_on_disk_failure:
                cr.failover.on_disk_failure.create()
            # The /configuration/version (config) leaf is updated when the
            # version changes. This is likely to cause out-of-sync.
            flags = ncs.maapi.COMMIT_NCS_NO_OUT_OF_SYNC_CHECK | ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE
            t_write.apply(flags=flags)


class NokiaSrosOperationsProto(Protocol):
    """Defines the interface for device operations (CLI or NETCONF)"""

    def get_version(self) -> str:
        ...

    def get_uptime(self) -> int:
        ...

    def get_free_space(self) -> Tuple[str, int]:
        ...

    def copy_file(self, src: str, dst: str) -> Tuple[bool, Optional[str]]:
        ...

    def is_bof_configured(self, name: str) -> bool:
        ...

    def get_redundancy_info(self) -> Tuple[str, str]:
        ...

    def redundancy_failover(self):
        ...

    def prepare_configure_bof(self, name: str) -> None:
        ...

    @staticmethod
    def cli_exec(d: ncs.maagic.Container, cmd: str, strip_prompt: bool = True) -> str:
        ...

    def prepare_format_standby(self) -> None:
        ...

    def prepare_save_rollback(self, name: str) -> None:
        ...

    def prepare_synchronize_bootenv(self) -> None:
        ...

    def reboot(self) -> bool:
        ...

    def calculate_checksum(self, path: str) -> str:
        ...


class NokiaSrosCliStrategy(NokiaSrosOperationsProto):
    def __init__(self, context, device: str):
        super().__init__()
        self.context = context
        self.device = device

    def get_version(self) -> str:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            return d.live_status.alu_stats__system.information.software_version

    def get_uptime(self) -> int:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            uptime = d.live_status.alu_stats__system.information.system_up_time
            return self._uptime_to_seconds(uptime)

    @staticmethod
    def _uptime_to_seconds(uptime: str) -> int:
        # 0 days, 01:15:29.42
        m = re.search(r'(?P<d>\d+) days, (?P<h>\d+):(?P<m>\d+):(?P<s>\d+)\.(?P<cs>\d+)', uptime)
        if not m:
            raise Exception(f'Unexpected uptime format: {uptime}')
        return int(m.group('d')) * 86400 + \
            int(m.group('h')) * 3600 + \
            int(m.group('m')) * 60 + \
            int(m.group('s'))

    def get_free_space(self) -> Tuple[str, int]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            r = self.cli_exec(d, 'file dir')
            m = re.search(r'(\d+) bytes free', r)
            if m is not None:
                return 'cf3:', int(m.groups()[0])
            else:
                raise Exception('Storage usage not found')

    def copy_file(self, src: str, dst: str) -> Tuple[bool, Optional[str]]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            r = self.cli_exec(d, f'file copy {src} {dst} force')
            if re.search(r'1 file copied', r) is None:
                return False, r
            else:
                return True, None

    def is_bof_configured(self, name: str) -> bool:
        with self.context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            return d.config.alu__bof.primary_image == name

    def get_redundancy_info(self) -> Tuple[str, str]:
        with self.context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            try:
                r = self.cli_exec(d, 'show redundancy synchronization', strip_prompt=False)
            except _ncs.error.Error as e:
                if 'Failed to connect to device' in str(e) or \
                    'External error in the NED implementation for device' in str(e):
                    raise CommunicationError(str(e)) from e
                else:
                    raise
            m = re.search(r'Standby Status\s+:\s*(.*)$', r, re.MULTILINE)
            if m:
                standby_status = m.groups()[0].strip()
            else:
                raise Exception('Could not read redundancy information')
            # The normal standby status in CLI is "standby ready" but in NETCONF
            # it is just "standby". We're forward looking so let's transform
            # the CLI status to a compatible value.
            standby_status = standby_status[:-len(' ready')]

            # the last line of the output is the prompt: A:901-R1-2050#
            # or *A:901-R1-2050#
            # the last letter before the colon is the active node
            active_node = r.splitlines()[-1].split(':')[0][-1]
            return active_node, standby_status

    def redundancy_failover(self):
        with self.context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            self.cli_exec(d, 'admin redundancy force-switchover')

    def prepare_configure_bof(self, name: str) -> None:
        # Due to the limitations of the ALU CLI NED, candidate-config must be
        # disabled for BOF changes
        with self.context.maapi.start_write_trans() as t_write:
            write_device = ncs.maagic.get_node(t_write, f'/devices/device{{{self.device}}}')
            current_setting = write_device.ned_settings.alu_meta__alu_sr.candidate_commit
            write_device.ned_settings.alu_meta__alu_sr.candidate_commit = 'disabled'
            t_write.apply(flags=ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE)

        with self.context.maapi.start_write_trans() as t_write:
            root = ncs.maagic.get_root(t_write)
            d = root.devices.device[self.device]
            d.sync_from()
            current_primary = d.config.alu__bof.primary_image
            d.config.alu__bof.primary_image = name
            d.config.alu__bof.secondary_image = current_primary
            t_write.apply()

        with self.context.maapi.start_write_trans() as t_write:
            write_device = ncs.maagic.get_node(t_write, f'/devices/device{{{self.device}}}')
            write_device.ned_settings.alu_meta__alu_sr.candidate_commit = current_setting
            t_write.apply(flags=ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE)

    @staticmethod
    def cli_exec(d: ncs.maagic.Container, cmd: str, strip_prompt: bool = True) -> str:
        cmd_input = d.live_status.alu_stats__exec.any.get_input()
        cmd_input.args = cmd.split(' ')
        r = d.live_status.alu_stats__exec.any(cmd_input).result
        # The last line of the output contains the prompt. Let's strip it and
        # return the remainder of the output.
        if strip_prompt:
            return '\n'.join(x.strip() for x in r.split('\n')[:-1])
        else:
            return r

    def prepare_format_standby(self) -> None:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            # The 'file shutdown' and 'file no shutdown' commands have no output
            r = self.cli_exec(d, 'file shutdown cf3-b:')
            if r:
                raise Exception("Error executing 'file shutdown cf3-b:' {r}")
            r = self.cli_exec(d, 'file format cf3-b:')
            if not re.search(r'Drive [^ ]+ is formatted', r):
                raise Exception("Error executing 'file format cf3-b:': {r}")
            # the 'file no shutdown command' has no output
            r = self.cli_exec(d, 'file no shutdown cf3-b:')
            if r:
                raise Exception("Error executing 'file shutdown cf3-b:' {r}")

    def prepare_save_rollback(self, name: str) -> None:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            r = self.cli_exec(d, f'admin rollback save rescue comment "Pre install {name}"')
            if re.search(r'^\s*Saving rollback configuration to.* OK\s*$', r.strip(), re.MULTILINE) is None:
                raise Exception(f'Error saving rollback configuration: {r}')

    def prepare_synchronize_bootenv(self) -> None:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            r = self.cli_exec(d, 'admin redundancy synchronize boot-env')
            if re.search(r'^Completed\.', r.strip(), re.MULTILINE) is None:
                raise Exception(f'Error synchronizing boot-env: {r}')

    def reboot(self) -> bool:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            try:
                self.cli_exec(d, 'admin reboot upgrade now')
            except Exception as e:
                # normal path for reboot, NSO immediately loses connection
                if str(e) == 'External error in the NED implementation for device dut-r1: read eof':
                    return True
                else:
                    raise
            return True

    def calculate_checksum(self, path: str) -> str:
        # TODO: file checksum was found missing in SROS 18. Check if it works in never versions ...
        raise NotImplementedError('No checksum operation available in the CLI?!')

class NokiaSrosNetconfStrategy(NokiaSrosOperationsProto):
    _NOKIA_NS_MAP = {
        'nc': 'urn:ietf:params:xml:ns:netconf:base:1.0',
        'bof-conf': 'urn:nokia.com:sros:ns:yang:sr:bof-conf'
    }

    def __init__(self, context, device: str):
        super().__init__()
        self.context = context
        self.device = device

    def get_version(self) -> str:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            return d.live_status.state__state.system.version.version_number

    def get_uptime(self) -> int:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            uptime = int(d.live_status.state__state.system.up_time / 1000)
            return uptime

    def get_free_space(self) -> Tuple[str, int]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            active_cpm = d.live_status.state__state.system.active_cpm_slot
            return 'cf3:', d.live_status.state__state.cpm[active_cpm].flash['3'].free_space * 1024

    def copy_file(self, src: str, dst: str) -> Tuple[bool, Optional[str]]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            copy_input = d.live_status.oper_file__file.copy.get_input()
            copy_input.source_url = src
            copy_input.destination_url = dst
            copy_input.force.create()
            try:
                r = d.live_status.oper_file__file.copy(copy_input)
                if r.status == 'completed':
                    return True, None
                else:
                    return False, '\n'.join(r.error_message)
            except Exception as e:
                return False, str(e)

    def is_bof_configured(self, name: str) -> bool:
        image_filter = f"""<bof xmlns="{self._NOKIA_NS_MAP['bof-conf']}">
  <image />
</bof>"""
        with self._ncclient() as m:
            try:
                images = m.get_config(source='bof-running', filter=('subtree', image_filter))
            except ncclient.operations.rpc.RPCError as err:
                raise Exception(f'RPC error: {err.message.strip()}') from err
            else:
                primary_image = images.xpath('/nc:rpc-reply/nc:data/bof-conf:bof/bof-conf:image/bof-conf:primary-location', namespaces=self._NOKIA_NS_MAP)[0].text
                return primary_image == name

    def get_redundancy_info(self) -> Tuple[str, str]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            standby_status = d.live_status.state__state.redundancy.synchronization.standby_status
            active_node = d.live_status.state__state.system.active_cpm_slot
            return active_node, standby_status

    def redundancy_failover(self):
        with self.context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            switchover_input = d.live_status.oper_admin__admin.redundancy.force_switchover.get_input()
            switchover_input.now.create()
            r = d.live_status.oper_admin__admin.redundancy.force_switchover(switchover_input)
            if r.status != 'completed':
                raise Exception(f'Error forcing switchover: {"".join(r.error_message)}')

    def prepare_configure_bof(self, name: str) -> None:
        image_filter = f"""<bof xmlns="{self._NOKIA_NS_MAP['bof-conf']}">
  <image />
</bof>"""
        with self._ncclient() as m:
            try:
                images = m.get_config(source='bof-running', filter=('subtree', image_filter))
                primary_image = images.xpath('/nc:rpc-reply/nc:data/bof-conf:bof/bof-conf:image/bof-conf:primary-location', namespaces=self._NOKIA_NS_MAP)[0].text
                image_swap = f"""<config>
  <bof xmlns="{self._NOKIA_NS_MAP['bof-conf']}">
    <image>
      <primary-location>{name}</primary-location>
      <secondary-location>{primary_image}</secondary-location>
    </image>
  </bof>
</config>"""
                m.lock(target='bof-candidate')
                m.edit_config(target='bof-candidate', config=image_swap, error_option='rollback-on-error', test_option='test-then-set')
                m.validate(source='bof-candidate')
                # Need to create custom RPC here for commit to include <configuration-region>
                commit = """<commit>
  <configuration-region>bof</configuration-region>
</commit>"""
                m.rpc(ncclient.xml_.to_ele(commit))
                m.unlock(target='bof-candidate')
            except ncclient.operations.rpc.RPCError as err:
                raise Exception(f'RPC error: {err.message.strip()}') from err

    @staticmethod
    def cli_exec(d: ncs.maagic.Container, cmd: str, _: bool = True) -> str:
        cmd_input = d.live_status.oper_global__global_operations.md_cli_raw_command.get_input()
        cmd_input.md_cli_input_line = cmd
        r = d.live_status.oper_global__global_operations.md_cli_raw_command(cmd_input).results.md_cli_output_block
        return r

    def prepare_format_standby(self) -> None:
        # /file/format action currently does not work - requires confirmation?!
        # This is a bug, let's skip it for now:
        # https://gitlab.dev.terastrm.net/TeraStream/vendor-issues/-/issues/46
        # pylint: disable=unreachable
        raise NotImplementedError("The '/file/format' action requires confirmation")
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            file_input = d.live_status.oper_file__file.disable.get_input()
            file_input.cflash_id = 'cf3-b:'
            r = d.live_status.oper_file__file.disable(file_input)
            if r.status != 'completed':
                raise Exception(f'Error disabling standby flash: {"".join(r.error_message)}')

            r = d.live_status.oper_file__file.format(file_input)
            if r.status != 'completed':
                raise Exception(f'Error formatting standby flash: {"".join(r.error_message)}')

            r = d.live_status.oper_file__file.enable(file_input)
            if r.status != 'completed':
                raise Exception(f'Error enable standby flash: {"".join(r.error_message)}')

    def prepare_save_rollback(self, _: str) -> None:
        raise NotImplementedError("'admin rollback save rescue' command not implemented as YANG action")

    def prepare_synchronize_bootenv(self) -> None:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            sync_input = d.live_status.oper_admin__admin.redundancy.synchronize.get_input()
            sync_input.boot_environment.create()
            r = d.live_status.oper_admin__admin.redundancy.synchronize(sync_input)
            if r.status != 'completed':
                raise Exception(f'Error synchronizing boot-env: {"".join(r.error_message)}')

    def reboot(self) -> bool:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            reboot_input = d.live_status.oper_admin__admin.reboot.get_input()
            reboot_input.now.create()
            reboot_input.card = 'upgrade'
            try:
                r = d.live_status.oper_admin__admin.reboot(reboot_input)
            except Exception as e:
                # normal path for reboot, NSO immediately loses connection
                if 'Operation deleted - session terminated' in str(e) \
                   or 'transport timeout; closing session' in str(e):
                    return True
                else:
                    raise
            raise Exception(f'Error rebooting for upgrade: {"".join(r.error_message)}')

    def _ncclient(self) -> ncclient.manager.Manager:
        credentials = DeviceCredentials.from_cdb(self.context, self.device)
        if credentials is None:
            raise Exception(f'Device credentials for {self.device} could not be retrieved')

        params = {
            'host': credentials.address,
            'port': credentials.port,
            'username': credentials.username,
            'hostkey_verify': False,
            'device_params': {'name': 'sros'}
        }
        if credentials.rsa_private_key:
            # Must create a file for the key?! https://github.com/ncclient/ncclient/issues/192
            # This is some paramiko weirdness leaking out through the ncclient API. Check if it
            # is possible to pass a StreamIO instead of a file, like we do in device-automaton ...
            raise NotImplementedError('ncclient SSH passwordless authentication not implemented yet')
        else:
            params['password'] = credentials.password

        self.context.log.info(params)
        return ncclient.manager.connect(**params)

    def calculate_checksum(self, path: str) -> str:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            checksum_input = d.live_status.oper_file__file.checksum.get_input()
            checksum_input.type = 'sha256'
            checksum_input.url = path
            r = d.live_status.oper_file__file.checksum(checksum_input)
            if r.status == 'completed':
                return r.results.checksum_value
            else:
                raise Exception(f'Error getting checksum for file {path}: {"".join(r.error_message)}')


class NokiaSrosOperations(DeviceOperations):
    op: NokiaSrosOperationsProto

    def __init__(self, context, device: str, vr: bool = False):
        super().__init__(context, device, vr)
        # Check if the device supports the YANG modules for structured oper data and actions
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as oper_t_read:
            d = ncs.maagic.get_node(oper_t_read, f'/devices/device{{{device}}}')
            if 'urn:nokia.com:sros:ns:yang:sr:oper-global' in d.capability:
                self.op = NokiaSrosNetconfStrategy(context, device)
            else:
                self.op = NokiaSrosCliStrategy(context, device)

    def validate_platform(self) -> Optional[str]:
        return self._validate_platform((device_os.DeviceOs.SROS_CLI, device_os.DeviceOs.SROS_NC))

    def is_virtual_router(self) -> bool:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            return d.platform.serial_number == 'vSIM'

    def get_version(self) -> str:
        return self.op.get_version()

    def get_boot_time(self) -> datetime:
        uptime = self.op.get_uptime()
        return datetime.utcnow() - timedelta(seconds=uptime)

    def get_free_space(self) -> Tuple[str, int]:
        return self.op.get_free_space()

    def copy_image(self, name: str, destination_volume: str) -> str:
        self.create_dir(destination_volume)
        dst = f'{destination_volume}/{pathlib.Path(name).name}'
        self.scp_to(str(name), dst)
        # file checksum not available on device ...
        return dst

    def prepare_copy_bootldr(self, name: str) -> None:
        result, error = self.op.copy_file(f'{name}\\boot.ldr', 'cf3:\\boot.ldr')
        if not result:
            raise Exception(f'Error copying boot.ldr to active: {error}')
        result, error = self.op.copy_file(f'{name}\\boot.ldr', 'cf3-b:\\boot.ldr')
        if not result:
            raise Exception(f'Error copying boot.ldr to standby: {error}')

    def is_in_sync(self) -> bool:
        with self.context.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            d = root.devices.device[self.device]
            result = d.check_sync()
            return result.result.string == 'in-sync'

    def is_bof_configured(self, name: str) -> bool:
        return self.op.is_bof_configured(name)

    def get_redundancy_info(self) -> Tuple[str, str]:
        return self.op.get_redundancy_info()

    def redundancy_failover(self):
        self.op.redundancy_failover()

    def prepare_configure_bof(self, name: str) -> None:
        self.op.prepare_configure_bof(name)

    def prepare_format_standby(self) -> None:
        self.op.prepare_format_standby()

    def prepare_save_rollback(self, name: str) -> None:
        self.op.prepare_save_rollback(name)

    def prepare_synchronize_bootenv(self) -> None:
        self.op.prepare_synchronize_bootenv()

    def reboot(self) -> bool:
        return self.op.reboot()

    def calculate_checksum(self, path: str) -> str:
        return self.op.calculate_checksum(path)


@dataclasses.dataclass(frozen=True)
class CiscoInstallOperationResult:
    result: bool
    op_id: Optional[int]
    op_log: Optional[str]
    error: Optional[str]


class CiscoIosXrOperations(DeviceOperations):
    def validate_platform(self) -> Optional[str]:
        return self._validate_platform((device_os.DeviceOs.IOSXR,))

    def get_boot_time(self) -> datetime:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            uptime = d.live_status.shellutil_oper__system_time.uptime.uptime
            now = datetime.utcnow()
            # always round down to the nearest second
            boot_time = now - timedelta(seconds=uptime, microseconds=now.microsecond)
            return boot_time

    def get_committed_version(self, package_name: str = None) -> Optional[str]:
        """Get package or base software version for a committed component.

        A committed component is already active (in use), and will remain so
        even after a reboot.

        :param package_name: optional name of the package
        """
        return self._get_version('committed', package_name)

    def get_active_version(self, package_name: str = None) -> Optional[str]:
        """Get package or base software version for an active component.

        An active component is currently in use, but will be deactivated in
        the event of a reboot.

        :param package_name: optional name of the package
        """
        return self._get_version('active', package_name)

    def _get_version(self, sw_list_name: str, package_name: str = None) -> Optional[str]:
        """Get package version info from 'active' or 'committed' software list

        The internal method determines which YANG model is supported on the
        device, then attempts to find the base or package software version
        from the 'active' or 'committed' list.
        """
        if sw_list_name not in ('active', 'committed'):
            raise ValueError(f'Unexpected sw_list name: {sw_list_name}')
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]

            if 'http://cisco.com/ns/yang/Cisco-IOS-XR-installmgr-admin-oper' in d.capability:
                # The device supports the module with structured active/committed package information:
                #     DEVICE
                #     NAME    NAME                             VERSION   BUILD INFORMATION
                #     -------------------------------------------------------------------------------------------------------------------------------------------
                #     disk0:  iosxr-infra-6.4.2.CSCvn15572     1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-px-6.4.2.CSCvn15572        1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  iosxr-mgbl-6.4.2.CSCvn20544      1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-px-6.4.2.CSCvn20544        1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-fwding-6.4.2.CSCvm03857    0.0.53.i  Built by jwu in sjc-ads-8326:/nobackup/SMU_BLD_WS/CSCvm03857.181126165124 for pie.ppc
                #     disk0:  asr9k-base-6.4.2.CSCvm03857      0.0.53.i  Built by jwu in sjc-ads-8326:/nobackup/SMU_BLD_WS/CSCvm03857.181126165124 for pie.ppc
                #     disk0:  asr9k-px-6.4.2.CSCvm03857        0.0.53.i  Built by jwu in sjc-ads-8326:/nobackup/SMU_BLD_WS/CSCvm03857.181126165124 for pie.ppc
                #     disk0:  asr9k-base-6.4.2.CSCvk68799      1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-fpd-6.4.2.CSCvk68799       1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-px-6.4.2.CSCvk68799        1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  iosxr-security-6.4.2.CSCvk51963  1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-px-6.4.2.CSCvk51963        1.0.0     Built by sajshah in iox-lnx-smu3:/san1/EFR/smu_r64x_6_4_2/workspace for pie.ppc
                #     disk0:  asr9k-optics-supp                6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  asr9k-optic-px                   6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-infra                      6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-fwding                     6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-routing                    6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-diags                      6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-ce                         6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-gcp-fwding                 6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  iosxr-common-pd-fib              6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  asr9k-os-mbi                     6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                #     disk0:  asr9k-base                       6.4.2     Built by radharan in iox-lnx-051:/auto/srcarchive17/prod/6.4.2/asr9k-px/ws for pie.ppc
                # asr9k-base is the base software version

                sw_list = getattr(d.live_status.install_mgr_admin_oper__install.software_inventory, sw_list_name)
                for package in sw_list.summary.default_load_path:
                    if package_name and package.name == package_name:
                        # we're looking for a specific SMU
                        return package.version
                    elif not package_name and package.name == 'asr9k-base':
                        # we're looking for the base software version
                        return package.version

            elif 'http://cisco.com/ns/yang/Cisco-IOS-XR-spirit-install-instmgr-oper' in d.capability:
                # The device supports the module with unstructured package information
                # admin@ncs> show devices device xrv651 live-status software-install active-summary | notab
                # live-status software-install active-summary active-package-info
                #  number-of-active-packages 2
                #  active-packages           "        xrv9k-xr-6.5.1 version=6.5.1 [Boot image]\n        xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679\n"
                #
                # The node 'committed-package-info' is an unkeyed list with a single entry.
                # (same goes for 'active-package-info)
                sw_list = getattr(d.live_status.spirit_install_instmgr_oper__software_install, f'{sw_list_name}_summary')
                package_info = getattr(sw_list, f'{sw_list_name}_package_info')
                for pi in package_info:
                    # Child node '(committed|active)-packages' contains an unstructured list of packages.
                    packages = getattr(pi, f'{sw_list_name}_packages')
                    if package_name:
                        # Find the literal or partial package name, line example:
                        # xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679
                        m = re.search(rf'\s*({package_name}[^\s]*)', packages, re.MULTILINE)
                    else:
                        # Find the base software version, line example:
                        # xrv9k-xr-6.5.1 version=6.5.1 [Boot image]
                        m = re.search(r'(\S+)\s+version=[0-9.]+\s+\[Boot image\]', packages)

                    if m is not None:
                        return m.groups()[0]
            else:
                raise Exception('No supported YANG module found for listing package versions')
        return None

    def get_current_install_request(self) -> Optional[str]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            r = d.live_status.spirit_install_instmgr_oper__software_install.request.curr_inst_oper
            if r != 'No install operation in progress':
                return r
            else:
                return None

    def get_free_space(self) -> Tuple[str, int]:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            root = ncs.maagic.get_root(t)
            d = root.devices.device[self.device]
            # The 'node' list contains 0/RSP0/CPU0, 0/RSP1/CPU0 and so on. We pick the first
            # 'RSP'-type node and within it a 'harddisk' type filesystem with the most available space
            #
            # mzagozen@bn831x2a-oss2-lab-nso> show devices device 901-R1-2054 live-status shellutil-filesystem-oper:file-system
            # NODE NAME    SIZE         FREE         TYPE        FLAGS  PREFIXES
            # ---------------------------------------------------------------------------------
            # 0/RSP0/CPU0            -            -  network     rw     qsm/dev/fs/tftp:
            #                        -            -  network     rw     qsm/dev/fs/rcp:
            #                        -            -  network     rw     qsm/dev/fs/ftp:
            #               5122293760   5115931136  dumper-lnk  rw     qsm/dumper_disk0a:
            #               5122293760   5121016832  dumper-lnk  rw     qsm/dumper_disk1a:
            #               6408880128   4363035648  dumper-lnk  rw     qsm/dumper_harddisk:
            #                767090688    767082496  dumper-lnk  rw     qsm/dumper_harddiskb:
            #                801112064    800323584  dumper-lnk  rw     qsm/dumper_harddiska:
            #              25612500992  25606218240  dumper-lnk  rw     qsm/dumper_disk1:
            #              25612500992  20183217664  dumper-lnk  rw     qsm/dumper_disk0:
            #               6408880128   4363035648  harddisk    rw     harddisk:
            #                801112064    800323584  harddisk    rw     harddiska:
            #                767090688    767082496  harddisk    rw     harddiskb:
            #              25612500992  20183217664  flash-disk  rw     disk0:
            #              25612500992  25606218240  flash-disk  rw     disk1:
            #               5122293760   5115931136  flash-disk  rw     disk0a:
            #               5122293760   5121016832  flash-disk  rw     disk1a:
            #                   384000       352256  nvram       rw     nvram:
            # 0/RSP1/CPU0            -            -  network     rw     qsm/dev/fs/tftp:
            #                        -            -  network     rw     qsm/dev/fs/rcp:
            #                        -            -  network     rw     qsm/dev/fs/ftp:
            #               5122293760   5115931136  dumper-lnk  rw     qsm/dumper_disk0a:
            #               5122293760   5121016832  dumper-lnk  rw     qsm/dumper_disk1a:
            #               6408880128   4363035648  dumper-lnk  rw     qsm/dumper_harddisk:
            #                767090688    767082496  dumper-lnk  rw     qsm/dumper_harddiskb:
            #                801112064    800323584  dumper-lnk  rw     qsm/dumper_harddiska:
            #              25612500992  25606218240  dumper-lnk  rw     qsm/dumper_disk1:
            #              25612500992  20183217664  dumper-lnk  rw     qsm/dumper_disk0:
            #               6408880128   6398227456  harddisk    rw     harddisk:
            #                801112064    800009216  harddisk    rw     harddiska:
            #                767090688    767082496  harddisk    rw     harddiskb:
            #              25612500992  22505740800  flash-disk  rw     disk0:
            #              25612500992  25606218240  flash-disk  rw     disk1:
            #               5122293760   5121005056  flash-disk  rw     disk0a:
            #               5122293760   5121016832  flash-disk  rw     disk1a:
            for node in d.live_status.shellutil_filesystem_oper__file_system.node:
                if not re.search(r'RS?P', node.node_name):
                    continue
                disks = []
                for fs in node.file_system:
                    if fs.type == 'harddisk':
                        disks.append((fs.prefixes, int(fs.free)))
                # sort the list by free space in descending order
                most_free = sorted(disks, key=lambda x: x[1], reverse=True)
                return most_free[0]
        raise Exception('Storage usage not found')

    def copy_image(self, name: str, destination_volume: str, validate: bool = True) -> str:
        filename = pathlib.Path(name).name
        dst = str(pathlib.Path(destination_volume) / filename)
        self.scp_to(name, dst)

        if validate:
            pass
            # file checksum not available on device ...
        return dst

    def prepare_add(self, name_or_names: Union[Iterable[str], str]) -> CiscoInstallOperationResult:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            add_input = d.rpc.install_act__rpc_install_add.install_add.get_input()
            if isinstance(name_or_names, str):
                path = pathlib.Path(name_or_names).parent
                add_input.packagepath = path
                add_input.packagename = [pathlib.Path(name_or_names).name]
            else:
                distinct_paths = set()
                packages = set()
                for name in name_or_names:
                    distinct_paths.add(pathlib.Path(name).parent)
                    packages.add(pathlib.Path(name).name)
                if len(distinct_paths) > 1:
                    return CiscoInstallOperationResult(False, None, None, 'All packages must be placed in the same folder')
                add_input.packagepath = distinct_paths.pop()
                add_input.packagename = list(packages)

            r = d.rpc.install_act__rpc_install_add.install_add(add_input)
            if r.Error:
                return CiscoInstallOperationResult(False, None, None, r.Error)
            op_id = r.op_id
            print(f'install add op-id: {op_id}')

        # now monitor the asynchronous operation for success in the request log
        result, op_log, error = self._monitor_operation_log(op_id)
        return CiscoInstallOperationResult(result, op_id, op_log, error)

    def prepare_clean(self) -> CiscoInstallOperationResult:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            prepare_input = d.rpc.install_act__rpc_install_prepare.install_prepare.get_input()
            prepare_input.clean.create()
            r = d.rpc.install_act__rpc_install_prepare.install_prepare(prepare_input)
            if r.Error:
                return CiscoInstallOperationResult(False, None, None, r.Error)
            print(f'install prepare clean op-id: {r.op_id}')

        # now monitor the asynchronous operation for success in the request log
        result, op_log, error = self._monitor_operation_log(r.op_id)
        if result:
            return CiscoInstallOperationResult(result, r.op_id, op_log, error)
        else:
            # nothing to clean up, mark the operation as success
            if op_log and 'No pending install prepare operation found, abort this operation' in op_log:
                return CiscoInstallOperationResult(True, r.op_id, op_log, error)
            else:
                return CiscoInstallOperationResult(False, r.op_id, op_log, error)

    def prepare_prepare(self, add_op_id: int) -> CiscoInstallOperationResult:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            prepare_input = d.rpc.install_act__rpc_install_prepare.install_prepare.get_input()
            prepare_input.ids.id_no = [add_op_id]
            r = d.rpc.install_act__rpc_install_prepare.install_prepare(prepare_input)
            if r.Error:
                return CiscoInstallOperationResult(False, None, None, r.Error)
            print(f'install prepare op-id: {r.op_id}')

        # now monitor the asynchronous operation for success in the request log
        # prepare could take "a while" when upgrading base version ...
        result, op_log, error = self._monitor_operation_log(r.op_id, timeout=600)
        return CiscoInstallOperationResult(result, r.op_id, op_log, error)

    def activate(self) -> CiscoInstallOperationResult:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            activate_input = d.rpc.install_act__rpc_install_activate.install_activate.get_input()
            activate_input.activate_prepared_pkg.create()
            r = d.rpc.install_act__rpc_install_activate.install_activate(activate_input)
            if r.Error:
                return CiscoInstallOperationResult(False, None, None, r.Error)
            print(f'install activate op-id: {r.op_id}')

        # now monitor the asynchronous operation for success in the request log
        result, op_log, error = self._monitor_operation_log(r.op_id)
        return CiscoInstallOperationResult(result, r.op_id, op_log, error)

    def commit(self) -> CiscoInstallOperationResult:
        with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
            d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
            commit_input = d.rpc.install_act__rpc_install_commit.install_commit.get_input()
            # commit_input.sdr.create()
            r = d.rpc.install_act__rpc_install_commit.install_commit(commit_input)
            if r.Error:
                return CiscoInstallOperationResult(False, None, None, r.Error)
            print(f'install commit op-id: {r.op_id}')

        # now monitor the asynchronous operation for success in the request log
        result, op_log, error = self._monitor_operation_log(r.op_id)
        return CiscoInstallOperationResult(result, r.op_id, op_log, error)

    def fetch_install_operation_log(self, op_id) -> Tuple[bool, bool, Optional[str], Optional[str]]:
        """Fetch and evaluate an install operation log

        The method will attempt to retrieve the log from the device (show install log <op_id>).
        Transient errors like:
         - the install log doesn't exist yet,
         - device connections error,
         - the install operation is not done yet.
        will be retried up to 3 times before returning the final state

        :param op_id: install operation request ID
        :return: tuple of (request_done?, success?, request_log, error)
        """
        retries = 0
        op_log: Optional[str]
        exc: Optional[Exception]
        # Retry loop for ruling out transient errors, like connection error, the log
        # not being created yet, ...
        while retries < 3:
            op_log = None
            exc = None
            try:
                with self.context.maapi.start_read_trans(db=ncs.OPERATIONAL) as t:
                    time.sleep(1)
                    d = ncs.maagic.get_node(t, f'/devices/device{{{self.device}}}')
                    op_log = d.live_status.spirit_install_instmgr_oper__software_install.operation_logs.operation_log[str(op_id)].detail.log
                    if op_log is None:
                        continue
                    # If the install * command was just executed, the install log may
                    # not exist yet
                    if re.search(r'^Install log not found for operation id \d+', op_log):
                        continue
                    # The $ in the regex matches the end of the string (and the log)
                    if re.search(r'Ending operation \d+$', op_log):
                        break
            except Exception as e:
                print(f'Error retrieving log for install op {op_id}: {e}')
                exc = e
            finally:
                retries += 1

        if op_log:
            # The $ in the regex matches the end of the string (and the log)
            if not re.search(r'Ending operation \d+$', op_log):
                return False, False, op_log, 'install request still in progress'
            # Now check the status of the operation - the 'Install operation X ...' line may appear
            # multiple times. We're looking for the last one.
            s = re.findall(r'Install operation \d+ (.*)$', op_log, re.MULTILINE)
            if not s:
                return False, False, op_log, f'could not find operation {op_id} result'
            # Look at the last match
            result = s[-1]
            if result == 'finished successfully':
                return True, True, op_log, None
            else:
                return True, False, op_log, result
        else:
            if exc:
                reason = f'exception {exc}'
            else:
                reason = 'retry count exceeded'
            return False, False, op_log, reason

    def _monitor_operation_log(self, op_id: int, timeout: int = 600) -> Tuple[bool, Optional[str], Optional[str]]:
        op_log = None

        # Wait until the operation is completed by searching for "Ending operation X" at the very
        # end of the log.
        start = timeit.default_timer()
        while True:
            if start + timeout < timeit.default_timer():
                return False, op_log, f'timeout {timeout} exceeded'
            done, success, op_log, error = self.fetch_install_operation_log(op_id)
            print(f'done={done}, success={success} op_log={op_log} error={error}')
            if done:
                break
        return success, op_log, error

    @staticmethod
    def get_file_packages(file: str) -> Iterator[str]:
        """Retrieve the packages contained in the tar file"""
        with tarfile.open(file) as t:
            names = (pathlib.Path(p).name for p in t.getnames())
            yield from (re.sub(r'(\.x86_64)?\.rpm$', '', n) for n in names if n.endswith('.rpm'))

    @classmethod
    def get_version_from_iso(cls, iso_filename: str) -> str:
        iso = pycdlib.PyCdlib()
        iso.open(iso_filename)
        with io.BytesIO() as fp:
            iso.get_file_from_iso_fp(iso_path='/IOSXR_IM.YML;1', outfp=fp)
            y = fp.getvalue()
        iso.close()
        return cls.version_from_yaml(y.decode('utf-8'))

    @staticmethod
    def version_from_yaml(iosxr_image_mdata: str) -> str:
        y = yaml.safe_load(iosxr_image_mdata)
        try:
            r = next(x for x in y['iso_rpms'] if x['iso_type'] == 'xr')
        except StopIteration as e:
            raise ValueError('IOS XR version not found in ISO') from e
        else:
            return r['name']

    @staticmethod
    def extract_package_info_from_log(op_log: str) -> Iterator[str]:
        reading = False
        for line in op_log.splitlines():
            # strip the timestamp
            line = re.sub(r'^\w+ \d+ \d+:\d+:\d+ ', '', line)
            if re.search(r'(Packages added:|Packages skipped as they are already in the repository:)\s*$', line):
                reading = True
            elif re.search(r'^ {4,}[^ ]+$', line) and reading:
                # package name is now the only token on the line
                yield re.sub(r'\.x86_64$', '', line.strip())
            else:
                reading = False

    @staticmethod
    def activate_requires_reload(op_log: str) -> bool:
        """Examine the 'install activate' operation log to determine if reload is required"""

        # base software:
        #   prepare: The prepared software is set to be activated with reload upgrade
        #   activate: The software will be activated with reload upgrade
        # SMU:
        #   prepare: The prepared software is set to be activated with process restart
        #   activate: The software will be activated with process restart
        if re.search(r'reload upgrade\s*$', op_log, re.MULTILINE):
            return True
        else:
            return False


def _main():
    class Context:
        def __init__(self, maapi, log):
            self.maapi = maapi
            self.log = log

    with ncs.maapi.Maapi() as m:
        with ncs.maapi.Session(m, 'software-install-script', 'system'):
            c = Context(m, logging.getLogger())
            o = JuniperJunosOperations(c, 'dut')
            print(o.list_files('/var/tmp/'))


if __name__ == '__main__':
    _main()
