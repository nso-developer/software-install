import dataclasses
import datetime
import importlib
from collections import OrderedDict
from types import ModuleType
from typing import (Iterable, Iterator, MutableMapping, Optional, Reversible,
                    Tuple, Type, cast)

import ncs

from .context import SoftwarePack, SoftwarePackComponent, SoftwarePackOs, State
from .step_executor_classes import StepExecutor


def _find_executor_module(os: SoftwarePackOs) -> ModuleType:
    os_module_map = {SoftwarePackOs.IOSXR: '.cisco',
                     SoftwarePackOs.JUNOS: '.junos',
                     SoftwarePackOs.SROS: '.sros'}
    return importlib.import_module(os_module_map[os], 'software_install')


def _get_component_steps(spc: SoftwarePackComponent, state: State) -> Iterable[Type[StepExecutor]]:
    executor_module = _find_executor_module(spc.os)
    get_steps_fn = getattr(executor_module, 'get_steps')
    return get_steps_fn(state)


@dataclasses.dataclass
class StepStatus:
    status: str
    needs_confirmation: bool = False


# Type alias for plan data structure. It is supposed to be
# OrderedDict[SoftwarePackComponent, OrderedDict[Type[StepExecutor], StepStatus]], but
# that doesn't sit well with mypy ...
TPlan = MutableMapping[SoftwarePackComponent, MutableMapping[Type[StepExecutor], StepStatus]]

class ComponentPlan:
    """An abstraction of the software install request plan

    This helps you manage the operational data for steps in the software
    component plan. Each step exposes operational state and log. With this
    class, the plan can be created or updated while the processor executes
    steps. If the steps change in future, this class will update the plan
    to the new version, taking care of preserving the state over time.
    """

    plan: TPlan
    confirm_steps: bool

    def __init__(self, read_request: ncs.maagic.Node):
        """Prepare the software pack components and their respective steps.

        :param read_request: a maagic node to the request software pack definition,
                             in a read config transaction
        """
        self.plan = OrderedDict()
        self.refresh_steps(read_request)

        self.error: Optional[str] = None
        self.failed_step: Optional[Tuple[SoftwarePackComponent, Type[StepExecutor]]] = None

    @property
    def _default_unprocessed(self) -> str:
        return 'waiting-confirmation' if self.confirm_steps else 'not-reached'

    def _mark_subsequent(self, component: SoftwarePackComponent, step: Type[StepExecutor], status: str):
        marker = False
        for component_step in self.plan[component].keys():
            if marker:
                self.plan[component][component_step].status = status
            elif component_step == step:
                marker = True

    def refresh_steps(self, read_request: ncs.maagic.Node):
        """Recalculate the list of steps given the current request state

        Depending on new information stored in the state object, the list of
        steps for this request may change. For instance, on Cisco IOS XR, the
        list of hardware components (FPDs) to upgrade and reload only becomes
        known after the base software packages are installed on the device. We
        currently only allow a request to _gain_ new steps, not remove them.

        This method also preserves the existing step statuses.
        """

        self.confirm_steps = read_request.confirm_steps

        new_plan: TPlan = OrderedDict()

        # TODO: refactor this to make read_request optional. We only need the
        # request data when this method is called from ComponentPlan.__init__()
        # in order to calculate the components. After the fact, components are
        # immutable and only steps within the component can change.

        sp = SoftwarePack.from_cdb(read_request.software_pack_data)

        for component in sp.components():
            state = State.from_component(read_request, component)
            new_plan[component] = OrderedDict()
            for step in _get_component_steps(component, state):
                # attempt to copy the step status from the current plan
                try:
                    status = self.plan[component][step]
                except KeyError:
                    # fallback to reading the status from the request
                    try:
                        read_step = read_request.component[component.name].step[step.name()]
                        needs_confirmation = False
                        if read_step.status == 'waiting-confirmation':
                            needs_confirmation = not read_step.confirmed.exists()
                        status = StepStatus(read_step.status, needs_confirmation)
                    except KeyError:
                        # finally default to unprocessed
                        status = StepStatus(self._default_unprocessed, self.confirm_steps)
                new_plan[component][step] = status

        # verify all existing components and their steps are present in the new
        # plan
        missing_components = set(c.name for c in self.plan.keys()) - set(c.name for c in new_plan.keys())
        if missing_components:
            raise ValueError(f'New plan is missing component(s): {missing_components}')
        for component in self.plan:
            missing_steps = set(s.name() for s in self.plan[component].keys()) - set(s.name() for s in new_plan[component].keys())
            if missing_steps:
                raise ValueError(f'New plan is missing component {component.name} step(s): {missing_steps}')

        self.plan = new_plan

    def next_step(self, component: SoftwarePackComponent) -> Iterator[Tuple[Type[StepExecutor], bool]]:
        """Yields the next available step in the plan and whether it can be executed

        The step status is needed to make a decision in the executor whether the
        step can be executed right now, or if it needs confirmation.
        """
        # cursor keeps track of the current step
        cursor = None
        # The plan for a component can change (new steps added). For example,
        # it is allowed to add new steps after the current step, or remove
        # them. Iterating with "for s in plan[component]" gives a consistent
        # view of the keys, excluding changes. The while loop and the cursor
        # keeping track of the current step ensure we're getting the correct
        # (updated) next step. The loop stops when reaching the last step of
        # the component.
        while cursor != list(self.plan[component].keys())[-1]:
            # marker is used to select the step *after* the cursor
            marker = False
            for k in self.plan[component]:
                if k == cursor:
                    # this is the step we yielded previously, set marker
                    marker = True
                    continue
                if marker or cursor is None:
                    # either we haven't yielded anyhing yet (first step) or
                    # this is the next step
                    cursor = k
                    needs_confirmation = self.plan[component][k].needs_confirmation
                    yield k, needs_confirmation
                    break

    def skip(self, component: SoftwarePackComponent, step: Type[StepExecutor]):
        self._set(component, step, 'skipped')

    def process(self, component: SoftwarePackComponent, step: Type[StepExecutor]):
        self._set(component, step, 'processing')

    def wait(self, component: SoftwarePackComponent, step: Type[StepExecutor]):
        self._set(component, step, 'wait')

    def complete(self, component: SoftwarePackComponent, step: Type[StepExecutor]):
        self._set(component, step, 'reached')

    def fail(self, component: SoftwarePackComponent, step: Type[StepExecutor], error: str = None):
        self._set(component, step, 'failed', error)

    def _set(self, component: SoftwarePackComponent, step: Type[StepExecutor], status: str, error: str = None):
        """Set a particular step state to the given status"""
        if error:
            self.error = error
            self.failed_step = (component, step)

        if self.plan[component][step].status == status:
            # same value, no-op & return
            return

        self.plan[component][step].status = status
        # if a step is set to 'failed', mark subsequent steps as unprocessed
        if status == 'failed':
            self._mark_subsequent(component, step, self._default_unprocessed)

    def write(self, write_sp: ncs.maagic.Node):
        """Write the plan to CDB

        :param write_sp: a maagic node to the device software pack, in a
                         writable (oper) transaction.
        """
        prev_component = None
        for component, steps in self.plan.items():
            plan_component = write_sp.component.create(component.name)

            prev_step = None
            for step, status in steps.items():
                step_name = step.name()
                plan_step = plan_component.step.create(step_name)
                changed = plan_step.status != status.status

                plan_step.status = status.status
                if (component, step) == self.failed_step:
                    plan_step.error = self.error
                else:
                    plan_step.error = None

                if changed and status.status != 'not-reached':
                    plan_step.when = datetime.datetime.now().isoformat()

                # reorder the steps according to our authoritative list
                if prev_step is None:
                    plan_component.step.move(step_name, ncs.maapi.MOVE_FIRST)
                else:
                    plan_component.step.move(step_name, ncs.maapi.MOVE_AFTER, prev_step)
                prev_step = step_name

            # remove steps that no longer exist, probably due to code changes
            auth_steps = set(step.name() for step in steps)
            cur_steps = set(step.name for step in plan_component.step)
            for step_name in (cur_steps - auth_steps):
                del plan_component.step[step_name]

            # reorder the components according to our authoritative list
            if prev_component is None:
                write_sp.component.move(component.name, ncs.maapi.MOVE_FIRST)
            else:
                write_sp.component.move(component.name, ncs.maapi.MOVE_AFTER, prev_component)
            prev_component = component.name

            # if the last step is 'reached', the component is completed
            last_step = next(reversed(cast(Reversible, steps)))
            plan_component.completed = steps[last_step].status == 'reached'

        # remove components that no longer exist, probably due to code changes
        auth_components = set(component.name for component in self.plan.keys())
        cur_components = set(component.name for component in write_sp.component)
        for component_name in (cur_components - auth_components):
            del write_sp.component[component_name]
