import logging
import time
import traceback
from typing import Callable, Optional, Tuple, Type

import ncs

from .component_plan import ComponentPlan
from .context import Context, SoftwarePackComponent, State
from .step_executor_classes import (StepExecutor, StepExecutorSupportsPreCheck,
                                    StepResult, RequestStatus)


def _execute_step_action(context: Context, executor_cls: Type[StepExecutor],
                         component: SoftwarePackComponent, plan: ComponentPlan,
                         executor: Callable[[State], StepResult], state: State) -> StepResult:
    with context.enter_step(executor_cls.name()):
        try:
            context.log.info(f'Executing {executor_cls.name()}.{executor.__name__}')

            with context.maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                plan.process(component, executor_cls)
                plan.write(ncs.maagic.get_node(oper_t_write, context.request_path))
                oper_t_write.apply()

            result = executor(state)
            context.log.info(f'Execution of {executor_cls.name()}.{executor.__name__} result {result}')
        except Exception as e:
            result = StepResult.FAILURE
            error = f'Error executing {executor_cls.name()}.{executor.__name__} - {type(e).__name__}: {e}'
            plan.fail(component, executor_cls, error=error)
            context.log.error(error)
            # include traceback in the log file
            context.log.debug(f'Error executing {executor_cls.name()}.{executor.__name__}:\n{traceback.format_exc()}')
        else:
            context.log.info(f'{executor_cls.name()}.{executor.__name__} result: {result}')
            if result == StepResult.FAILURE:
                plan.fail(component, executor_cls)
            elif result == StepResult.WAIT:
                plan.wait(component, executor_cls)
            elif result in (StepResult.SKIP_STEP, StepResult.SKIP_COMPONENT):
                plan.skip(component, executor_cls)
            else:
                plan.complete(component, executor_cls)

        with context.maapi.start_write_trans(db=ncs.OPERATIONAL) as t_write:
            if result != StepResult.FAILURE:
                state.flush(t_write)
            request = ncs.maagic.get_node(t_write, context.request_path)
            plan.refresh_steps(request)
            plan.write(ncs.maagic.get_node(t_write, context.request_path))
            t_write.apply()
        return result


def _execute_component(context: Context, plan: ComponentPlan, component: SoftwarePackComponent, state: State) -> Tuple[bool, Optional[StepResult]]:
    result: Optional[StepResult] = None
    next_step = None

    for executor_cls, needs_confirmation in plan.next_step(component):
        # next_step and result were set in the previous iteration of the inner loop.
        # If result is SKIP_COMPONENT, we need to set all remaining steps in this
        # component to 'skipped' status (and not execute them).
        # If next_step is set, it is set to a future step. Until this step is reached
        # in the loop, skip steps.
        if result == StepResult.SKIP_COMPONENT or \
            next_step and executor_cls.name() != next_step.name():
            plan.skip(component, executor_cls)
            continue
        # reset the loop variables
        result = None
        next_step = None

        if needs_confirmation:
            return True, None

        executor = executor_cls(context)
        if isinstance(executor, StepExecutorSupportsPreCheck):
            result = _execute_step_action(context, executor_cls, component, plan, executor.pre_check, state)
            if result in (StepResult.WAIT, StepResult.FAILURE):
                return False, result
            if result == StepResult.SKIP_COMPONENT:
                continue

        # Only run execute() if the step wasn't skipped
        if result is None or result == StepResult.SUCCESS:
            result = _execute_step_action(context, executor_cls, component, plan, executor.execute, state)
            if result in (StepResult.WAIT, StepResult.FAILURE):
                return False, result
            if result == StepResult.SKIP_COMPONENT:
                continue

        # Always run next_step() if applicable, even for skipped steps
        try:
            next_step = executor.next_step(state)
        except Exception as e:
            error = f'Error executing {executor_cls.name()}.next_step - {type(e).__name__}: {e}'
            plan.fail(component, executor_cls, error=error)
            context.log.error(error)
            # include traceback in the log file
            context.log.debug(f'Error executing {executor_cls.name()}.next_step:\n{traceback.format_exc()}')
            # even though the step was executed successfully, the next_step() method
            # failed, so we mark the step as failed as well
            return False, StepResult.FAILURE
    return False, result


def _write_request_status(context: Context, needs_confirmation: bool, result: Optional[StepResult] = None, obsolete: bool = False) -> RequestStatus:
    # if all components are completed, mark request as done
    # TODO: handle errors, waiting-confirm ...
    with context.maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
        request = ncs.maagic.get_node(oper_t_write, context.request_path)
        if obsolete:
            request_status = RequestStatus.OBSOLETE
            context.log.warn('Request is obsolete')
        else:
            completed = all(c.completed for c in request.component)
            if completed:
                request_status = RequestStatus.DONE
                del request.error_count
            elif needs_confirmation:
                request_status = RequestStatus.WAITING_CONFIRMATION
            elif result == StepResult.WAIT:
                request_status = RequestStatus.FAILED_TRANSIENT
                request.error_count.transient += 1
                request.error_count.other = 0
            else:
                request_status = RequestStatus.FAILED_OTHER
                request.error_count.other += 1
                request.error_count.transient = 0
            context.log.debug(f'Request status: {request.status}')

        request.status = request_status.value
        oper_t_write.apply()
        return request_status


def step_executor(context: Context, request_path: str) -> RequestStatus:
    # bump the current run-id
    with context.maapi.start_write_trans(db=ncs.OPERATIONAL) as t_write:
        request = ncs.maagic.get_node(t_write, request_path)
        run_id = request.run_id_count + 1
        request.run_id_count = run_id
        t_write.apply()

    context.run_id = run_id
    with context.maapi.start_read_trans() as t_read:
        request = ncs.maagic.get_node(t_read, request_path)
        context.request_path = request._path
        if request.obsolete:
            return _write_request_status(context, needs_confirmation=False, obsolete=True)
        plan = ComponentPlan(request)

    with context.maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
        plan.write(ncs.maagic.get_node(oper_t_write, request))
        ncs.maagic.get_node(oper_t_write, context.request_path).status = 'processing'
        oper_t_write.apply()

    result: Optional[StepResult]
    needs_confirmation: bool

    for component in plan.plan.keys():
        context.component = component.name
        with context.maapi.start_read_trans() as t_read:
            read_request = ncs.maagic.get_node(t_read, context.request_path)
            state = State.from_component(read_request, component)
        needs_confirmation, result = _execute_component(context, plan, component, state)

        if result in (StepResult.WAIT, StepResult.FAILURE) or needs_confirmation:
            break
    return _write_request_status(context, needs_confirmation=needs_confirmation, result=result)


def _create_request(device_name: str) -> int:
    """Helper for creating a new 'unprocessed' software install request"""
    with ncs.maapi.single_write_trans('python-step-executor', 'system', db=ncs.OPERATIONAL) as oper_t_write:
        d = ncs.maagic.get_node(oper_t_write, f'/devices/device{{{device_name}}}')
        sp = ncs.maagic.get_node(oper_t_write, f'/software-install/software-pack{{{d.software_pack.name}}}')

        # remove all current requests
        del d.software_pack.request
        r = d.software_pack.request.create('1')
        rsp = r.software_pack_data
        rsp.os = sp.os

        if sp.base.exists():
            rsp.base.create()
            rsp.base.version = sp.base.version
            rsp.base.filename = sp.base.filename

        for p in sp.patch:
            rp = rsp.patch.create(p.version)
            rp.filename = p.filename

        oper_t_write.apply()
    return 1


def _main():
    request_id = _create_request('dut')
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    start_time = time.time()
    request_path = f'/devices/device{{dut}}/software-pack/request{request_id}'
    with ncs.maapi.Maapi() as m, \
        ncs.maapi.Session(m, 'python-step-executor', 'system'), \
        Context(m, logging.getLogger('software-install')) as context:
        while step_executor(context, request_path) != RequestStatus.DONE:
            if time.time() - start_time > 45 * 60:
                print('Exceeded 45 min limit')
                break
            time.sleep(5)

if __name__ == '__main__':
    _main()
