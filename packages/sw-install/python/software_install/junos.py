from pathlib import Path
from functools import partialmethod
from typing import Iterable, Optional, Tuple, Type, no_type_check

from .context import Context, RouteEngine, StateJunos
from .software_install_script import JuniperJunosOperations, get_size, hash_file
from .step_executor_classes import StepExecutor, StepExecutorSupportsPreCheck, StepResult


SE = Type[StepExecutor]
@no_type_check
def _partial_RE(inner_cls: SE, re_id: int) -> SE:
    """Wrapper for creating new (parametrized) StepExecutor types

    The internals of the step executor rely on passing around StepExecutor
    classes (not instances!). This works out fine when the behavior of
    StepExecutor classes is only affected by the "state" object. In this case,
    we parametrize the types with re_id - routing engine id. As I'm writing
    this, I realize this smells very fishy. I plan on refactoring the whole
    state machine anyways, so let's just leave this be ...

    To keep the internals working, two types with the same parameter (re_id)
    value must be equivalent (= hash()). If we returned a new type every time
    the hashes would be different. As a workaround we turn the types into a
    singleton using a cache. Types are cached on the (RealStepExecutor, re_id)
    tuple."""
    class NewCls(inner_cls): # type: ignore
        __init__ = partialmethod(inner_cls.__init__, re_id=str(re_id))

        @classmethod
        def name(cls) -> str:
            """Display name of the type includes re_id"""
            return f'{inner_cls.__qualname__}[RE{re_id}]'

    # When "creating" a new parametrized type, we must ensure the hash()
    # function returns the same id if the re_id is the same. This makes it
    # possible to use the type as a dict key.
    if (inner_cls, re_id) not in _partial_RE._singles:
        _partial_RE._singles[inner_cls, re_id] = NewCls
    return _partial_RE._singles[inner_cls, re_id]
# Nothing to see here, just adding the _singles attribute to the function. The
# dictionary is used to cache parametrized types.
_partial_RE._singles = {}


def get_steps(state: StateJunos) -> Iterable[Type[StepExecutor]]:
    yield from (ValidatePlatform,
                CheckFiles)
    if state.dual_re:
        master, backup = state.route_engine_priority
    else:
        master, backup = (0,0)
    if state.dual_re:
        # perform the install steps on the backup node first
        yield from (_partial_RE(GetBootTime, backup),
                    _partial_RE(CheckVersions, backup),
                    _partial_RE(CopyImage, master),
                    _partial_RE(CopyImage, backup),
                    DisableFailover,
                    _partial_RE(PackageAdd, backup),
                    _partial_RE(Reboot, backup),
                    _partial_RE(Done, backup))
    yield from (_partial_RE(GetBootTime, master),
            _partial_RE(CheckVersions, master),
            _partial_RE(CopyImage, master),
            _partial_RE(PackageAdd, master),
            _partial_RE(Reboot, master),
            _partial_RE(Done, master))
    if state.dual_re:
        yield EnableFailover
    yield Done


def _get_device_ops(context: Context, state: StateJunos) -> JuniperJunosOperations:
    return JuniperJunosOperations(context, state.device, vr=state.virtual_router, switch=state.switch)


class ValidatePlatform(StepExecutor):
    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        result = o.validate_platform()
        if result:
            self.context.log.error(result)
            return StepResult.FAILURE
        re_info = o.get_route_engine_information()
        if not state.route_engine:
            for re_id in re_info:
                state.route_engine[str(re_id[0])] = RouteEngine()
                # the route_engine_priority list contains re slot ids, starting with master
                if re_id[2] == 'master':
                    state.route_engine_priority.insert(0, re_id[0])
                else:
                    state.route_engine_priority.append(re_id[0])
        dual_re = len(re_info) == 2
        if state.dual_re is not None and state.dual_re != dual_re:
            self.context.log.error(f'dual-re has changed: {state.dual_re} -> {dual_re}, request no longer valid')
            return StepResult.FAILURE
        else:
            state.dual_re = dual_re
        model = re_info[0][1]
        state.switch = model == 'QFX Routing Engine' or 'EX' in model
        if state.switch and state.dual_re:
            self._add_oper_log('Software install on switch with multiple FPCs is not supported')
            return StepResult.FAILURE
        state.virtual_router = model in ('RE-VMX', 'QFX Routing Engine')
        return StepResult.SUCCESS


class CheckFiles(StepExecutor):
    def execute(self, state: StateJunos) -> StepResult:
        for f in state.software_pack_component.filenames:
            if not Path(f).is_file():
                self._add_oper_log(f'File {f} does not exist!')
                return StepResult.FAILURE
        return StepResult.SUCCESS


class CheckVersions(StepExecutor):
    def __init__(self, context: Context, re_id: str):
        super().__init__(context)
        self.re_id = re_id

    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        version = o.get_version(int(self.re_id))
        state_re = state.route_engine[self.re_id]

        if state_re.done and state_re.version != version:
            # the target version was once running on this device (request was
            # done), but it has now changed?! let's assume we have to restart
            # the installation process
            state.reset()
        state_re.version = version

        if state_re.rebooted and state_re.boot_time:
            boot_time = o.get_boot_time(int(self.re_id))
            if boot_time > state_re.boot_time and state_re.version != state.software_pack_component.version:
                self.context.log.warning('Device rebooted, but new software version not active')
                return StepResult.FAILURE
        return StepResult.SUCCESS

    def next_step(self, state: StateJunos) -> Optional[Type[StepExecutor]]:
        state_re = state.route_engine[self.re_id]
        if state_re.version == state.software_pack_component.version:
            return _partial_RE(Done, self.re_id)
        elif state_re.rebooted:
            return _partial_RE(Reboot, self.re_id)
        return None

class GetBootTime(StepExecutor):
    def __init__(self, context: Context, re_id: str):
        super().__init__(context)
        self.re_id = re_id

    def execute(self, state: StateJunos) -> StepResult:
        if self.re_id not in state.route_engine:
            state.route_engine[self.re_id] = RouteEngine()
        state_re = state.route_engine[self.re_id]
        if state_re.boot_time is None:
            o = _get_device_ops(self.context, state)
            state_re.boot_time = o.get_boot_time(int(self.re_id))
        return StepResult.SUCCESS


# TODO: these steps are "equal" on all platforms. Replace this copypasta?
class CopyImage(StepExecutor, StepExecutorSupportsPreCheck):
    def __init__(self, context: Context, re_id: str):
        super().__init__(context)
        self.re_id = re_id

    def _is_file_present(self, state: StateJunos, local_file: str) -> Tuple[bool, Optional[str]]:
        o = _get_device_ops(self.context, state)
        expected_size = get_size(local_file)
        state_re = state.route_engine[self.re_id]
        assert(state_re.destination_volume is not None)
        files = o.list_files(state_re.destination_volume)
        filename = Path(local_file).name
        try:
            size = files[filename]
            if size != expected_size:
                return False, f'Expected size of {filename} {expected_size} does not match actual {size}'
        except KeyError:
            return False, f'File {filename} not found in {state_re.destination_volume}'

        local_hash = hash_file(local_file)
        remote_hash = o.calculate_checksum(str(Path(state_re.destination_volume) / filename))
        if local_hash != remote_hash:
            return False, f'File {filename} hash {remote_hash} does not match expected {local_hash}'

        return True, None

    def pre_check(self, state: StateJunos) -> StepResult:
        """Check if the image already exists, or there is enough available space for the image"""

        o = _get_device_ops(self.context, state)
        free_result = o.get_free_space(int(self.re_id))
        if free_result is None:
            self._add_oper_log('Could not get free space info from device')
            return StepResult.FAILURE

        state_re = state.route_engine[self.re_id]
        if not state_re.destination_volume:
            state_re.destination_volume, free = free_result
        else:
            _, free = free_result

        missing = set(state.software_pack_component.filenames)
        errors = []
        for f in state.software_pack_component.filenames:
            ok, error = self._is_file_present(state, f)
            if ok:
                missing.remove(f)
                if f not in state_re.destination_paths:
                    state_re.destination_paths[f] = str(Path(state_re.destination_volume) / Path(f).name)
            else:
                state_re.destination_paths.pop(f, None)
                errors.append(error)
        if errors:
            self.context.log.debug(errors)
        else:
            return StepResult.SKIP_STEP

        total_size = sum(get_size(f) for f in state.software_pack_component.filenames if f in missing)
        # The file we're copying must fit into the destination partition. It
        # will be extracted elsewhere, so there is no need for a buffer.
        if total_size > free:
            self._add_oper_log(f'Not enough available space: image={total_size}, free={free}')
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateJunos) -> StepResult:
        """Copy the image to destination"""
        state_re = state.route_engine[self.re_id]
        assert(state_re.destination_volume is not None)
        o = _get_device_ops(self.context, state)

        any_errors = False
        for f in state.software_pack_component.filenames:
            if f in state_re.destination_paths:
                continue

            if self.re_id == '0':
                state_re.destination_paths[f] = o.copy_image(f, state_re.destination_volume)
            else:
                source = 're0:' + state.route_engine['0'].destination_paths[f]
                o.file_copy(source, 're1:' + state_re.destination_volume)
                state_re.destination_paths[f] = state.route_engine['0'].destination_paths[f]

            present, error = self._is_file_present(state, f)
            if not present:
                assert(error is not None)
                self._add_oper_log(error)
                any_errors = True
        if any_errors:
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS


class DisableFailover(StepExecutor):
    def pre_check(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        current = o.check_failover()
        if current.configured:
            state.failover_config = current
            return StepResult.SUCCESS
        else:
            state.failover_config = None
            return StepResult.SKIP_STEP

    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        o.disable_failover()
        return StepResult.SUCCESS


class EnableFailover(StepExecutor):
    def pre_check(self, state: StateJunos) -> StepResult:
        if state.failover_config and state.failover_config.configured:
            return StepResult.SUCCESS
        else:
            return StepResult.SKIP_STEP

    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        o.enable_failover(state.failover_config)
        return StepResult.SUCCESS


class PackageAdd(StepExecutor):
    def __init__(self, context: Context, re_id: str):
        super().__init__(context)
        self.re_id = re_id

    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)

        for f in state.route_engine[self.re_id].destination_paths.values():
            self._add_oper_log(f'Executing "request package add {f} on re{self.re_id}"')
            result, message = o.package_add(f, int(self.re_id))
            self.context.log.debug(f'request package add {f} on re{self.re_id} result: {result}\n{message}')
            if not result:
                self.context.log.error(message)
                return StepResult.FAILURE
        return StepResult.SUCCESS


class Reboot(StepExecutor, StepExecutorSupportsPreCheck):
    def __init__(self, context: Context, re_id: str):
        super().__init__(context)
        self.re_id = re_id

    def pre_check(self, state: StateJunos) -> StepResult:
        state_re = state.route_engine[self.re_id]
        if state_re.rebooted and state_re.boot_time:
            o = _get_device_ops(self.context, state)
            boot_time = o.get_boot_time(int(self.re_id))
            if boot_time > state_re.boot_time:
                # device has rebooted
                version = o.get_version(int(self.re_id))
                if version != state.software_pack_component.version:
                    self.context.log.warning(f'Device rebooted, but new software version not active. Expected {state.software_pack_component.version}, running {version}')
                    return StepResult.FAILURE
                else:
                    return StepResult.SKIP_STEP
            else:
                return StepResult.WAIT
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateJunos) -> StepResult:
        o = _get_device_ops(self.context, state)
        if o.reboot(int(self.re_id)):
            state.route_engine[self.re_id].rebooted = True
            return StepResult.WAIT
        else:
            return StepResult.FAILURE


class Done(StepExecutor):
    def __init__(self, context: Context, re_id: Optional[str] = None):
        super().__init__(context)
        self.re_id = re_id

    def execute(self, state: StateJunos) -> StepResult:
        if self.re_id is None:
            state.done = True
        else:
            state.route_engine[self.re_id].done = True
        return StepResult.SUCCESS
