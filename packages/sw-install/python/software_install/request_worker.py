"""Software Install Request Worker

The request worker implements a producer / consumer(s) pattern, using the
`bgworker` package to manage processes. The producer (`cdb_scheduler`) and
consumers (`cdb_worker`) communicate using two queues:
- q1: producer -> consumer: producer puts jobs that need executing on the queue.
  A job is a tuple of (request_path, user_session_id).
- q2: consumer -> producer: after the job is processed (successfully or not),
  the worker places a tuple of (request_path, user_session_id, status) on the
  queue. The scheduler uses this information to decide if the request needs to
  be retried.

TODO:
- [ ] structure the jobs better
- [ ] monitor failed jobs in scheduler. what if a worker has died without
  reporting the status?

With parallel workers we risk executing multiple jobs affecting the same device.
While multiple jobs can be scheduled, a locking mechanism is in place that only
allows a single worker to process a job for a given device. We use partial CDB
locks built into NSO to lock the device path - /devices/device[name='foo']. In
addition to providing a locking API, using CDB locks has the additional benefit
in that it also prevents _others_ from making the changes to devices as well. We
want to block other workers / users while a software install request is in
progress.
"""

import contextlib
import logging
import multiprocessing
import os
import queue
import select
import signal
import time
from typing import Dict, Iterable, Iterator, Optional, Tuple

import ncs
from bgworker.background_process import Process

from .context import Context, OperCdbLoggingHandler
from .step_executor import step_executor
from .step_executor_classes import RequestStatus


class MaapiLocker:
    """MaapiLocker is a convenience wrapper around MAAPI partial lock

    NSO allows applications to acquire a global or partial CDB lock. The MAAPI
    function `lock_partial` is non-blocking and will return various error
    messages. This class simplifies locking with the use of a context manager.

        with MaapiLocker(maapi).lock("/devices/device[name='foo']", timeout=60) as (success, error):
            if not success:
                log.error(error)
            else:
                # do something

    """
    def __init__(self, maapi: ncs.maapi.Maapi):
        self.maapi = maapi

    @contextlib.contextmanager
    def lock(self, path: str, timeout: Optional[int] = None) -> Iterator[Tuple[bool, str]]:
        """Context-managed partial MAAPI lock

        :param path: string XPath for the partial lock
        :param timeout: optional timeout in seconds, will block
        :returns: tuple of (success, error_message)

        If the timeout parameter is not provided, the method will block until
        the lock is acquired.
        """
        lock_id = None
        start_time = time.time()
        while lock_id is None:
            if timeout and time.time() - start_time > timeout:
                yield False, 'timeout'
                return

            try:
                lock_id = self.maapi.lock_partial(ncs.RUNNING, [path])
            except ncs.error.Error as e:
                if e.confd_errno in (ncs.ERR_LOCKED, ncs.ERR_NOEXISTS):
                    print(e)
                    time.sleep(1)
                else:
                    yield False, str(e)
                    return

        # try..finally to make sure the lock is released even if an exception occurs
        # in the caller
        try:
            yield True, None
        finally:
            self.maapi.unlock_partial(lock_id)


class PeekableQueue(queue.PriorityQueue):
    def is_ready(self, threshold: float) -> bool:
        """Peek at the first element in the queue and check if it is ready"""
        if self.empty():
            return False
        # get the first (top) item off the queue and check the first element in
        # the tuple (scheduled time)
        return self.queue[0][0] < threshold


def cdb_scheduler(q1, q2):
    """Producer for software install request jobs

    :param q1: queue for passing jobs to workers
    :param q2: queue for receiving job statuses from workers and job triggers

    The producer (scheduler) receives job execution requests via q2. At the
    moment, only the `execute-request` action does that by placing a tuple
    (request_path, 'new') on the queue. this is a bit of a hack, but it works
    for a quick PoC. In future, we may want to react to new jobs with a
    subscriber??

    Once a job is "scheduled", the scheduler puts the job (tuple of
    (request_path, user_session_id)) on q1, the queue that is observed by the
    workers. One of the workers will pop the item off the queue and start
    processing. Upon completion, it will notify the scheduler via q2.

    After a job is processed, the scheduler evaluates the status. There is
    nothing to do if execution was successful. If the job failed, the scheduler
    schedules a retry if the running retry count is within configured limits.
    The job will be retried with a configurable backoff - exponential or
    constant.
    """
    log = logging.getLogger('cdb-scheduler')
    log.info('starting scheduler')
    waitroom = PeekableQueue()
    workers: Dict[str, int] = {}

    while True:
        rfds, _, _ = select.select([q2._reader], [], [], 1)
        for rfd in rfds:
            if rfd == q2._reader:
                job, usess_id, status = q2.get()
                log.info(f'got {job}, status={status}')
                if status == RequestStatus.CANCELLED:
                    # 1. find the worker in the map
                    # 2. kill / send a signal to the worker
                    if job in workers:
                        os.kill(workers[job], signal.SIGINT)
                        del workers[job]
                        with ncs.maapi.single_write_trans('python-sw-install-scheduler', 'system', db=ncs.OPERATIONAL) as t_write:
                            request = ncs.maagic.get_node(t_write, job)
                            request.status = RequestStatus.CANCELLED.value
                            t_write.apply()
                elif status == RequestStatus.PROCESSING:
                    # TODO: do not abuse usess_id for PID!!
                    workers[job] = usess_id
                elif status != RequestStatus.DONE:
                    try:
                        del workers[job]
                    except KeyError:
                        pass
                    if status in (RequestStatus.FAILED_OTHER, RequestStatus.FAILED_TRANSIENT):
                        with ncs.maapi.single_write_trans('python-sw-install-scheduler', 'system', db=ncs.OPERATIONAL) as t_write:
                            request = ncs.maagic.get_node(t_write, job)
                            error_count = request.error_count.transient + request.error_count.other
                            config = ncs.maagic.get_node(t_write, '/sw-install:software-install/error-handling')
                            if error_count > config.max_retries:
                                log.error(f'giving up on {job} (too many retries)')
                                continue
                            # backoff (global setting) is configured as a choice
                            # between 'factor' and 'constant'. let's try to read
                            # the 'factor' leaf first, then fall back to
                            # 'constant'.
                            if config.backoff.factor:
                                current_backoff = request.error_count.backoff or 10
                                backoff = current_backoff * config.backoff.factor
                            else:
                                backoff = config.backoff.constant
                            request.error_count.backoff = backoff
                            t_write.apply()

                        next_retry = time.time() + backoff
                        waitroom.put((next_retry, job, usess_id))
                    elif status == RequestStatus.UNPROCESSED:
                        # someone wants to execute this request now!
                        q1.put((job, usess_id))
                    else:
                        log.debug(f'no handler for status {status}')

        # let's see if a previously scheduled retry is ready ...
        if waitroom.is_ready(time.time()):
            _, job, usess_id = waitroom.get()
            log.info(f'retrying {job}')
            q1.put((job, usess_id))


def cdb_worker(n, q1, q2):
    """Consumer for software install request jobs

    :param n: worker numeric ID
    :param q1: queue for receiving jobs from the scheduler
    :param q2: queue for passing job results / progress to the scheduler

    The workers receive jobs from the scheduler via q1. The job is a tuple of
    (request_path, user_session_id). The worker first attempts to acquire a
    partial lock on the affected device. If timeout of 120s is exceeded, the job
    is marked as a failure and returned to the scheduler. If the lock is
    successfully acquired, the result of the executed job is likewise passed to
    the scheduler. This is done by enqueuing an item on q2.
    """
    log = logging.getLogger(f'cdb-worker{n}')
    log.debug('started')

    while True:
        try:
            job, usess_id = q1.get(timeout=1)
        except queue.Empty:
            continue
        with ncs.maapi.Maapi() as m:
            with ncs.maapi.Session(m, f'python-worker{n}', 'system'):
                q2.put((job, os.getpid(), RequestStatus.PROCESSING))
                # job is a path to a software install request -
                # /devices/device{foo}/software-pack/request{42}. let's get a
                # lock going on the device
                request = ncs.maagic.get_node(m, job)
                device_name = request._parent._parent._parent.name
                device_xpath = f"/ncs:devices/ncs:device[ncs:name='{device_name}']"

                with OperCdbLoggingHandler(log, user_session_id=usess_id), \
                     Context(m, log) as context, \
                     MaapiLocker(m).lock(device_xpath, timeout=120) as (success, reason):
                    if not success:
                        log.error(f'error acquiring lock {device_xpath}: {reason}')
                        q2.put((job, usess_id, 'lock-error'))
                        continue
                    result = step_executor(context, job)
                    log.info(f'{job}: result: {result}')
                    # send the job result to the scheduler
                    q2.put((job, usess_id, result))


def main(app, n_workers: int = 1) -> Tuple[multiprocessing.Queue, Iterable[Process]]:
    # Create two (multiprocessing) queues:
    # - q1: used to pass jobs from the scheduler to the workers
    # - q2: used to pass job status from the workers to the scheduler
    # We also use q2 to pass new jobs to the scheduler. This can change in
    # future to a subscriber?
    q1 = Process.new_queue()
    q2 = Process.new_queue()
    p_scheduler = Process(app, cdb_scheduler, (q1, q2))
    p_scheduler.start()
    processes = [p_scheduler]
    for n in range(n_workers):
        p_worker = Process(app, cdb_worker, (n, q1, q2))
        p_worker.start()
        processes.append(p_worker)

    return q2, processes


class RequestWorker(ncs.application.Application):
    workers: Iterable[Process]
    q: multiprocessing.Queue

    def setup(self):
        self.log.info('Starting RequestWorker')
        RequestWorker.q, self.workers = main(self, n_workers=2)

    def teardown(self):
        self.log.info('Tearing down RequestWorker')
        for w in self.workers:
            w.stop()

    @staticmethod
    def new_request(path, usess_id=0):
        RequestWorker.q.put((path, usess_id, RequestStatus.UNPROCESSED))

    @staticmethod
    def cancel_request(path, usess_id=0):
        RequestWorker.q.put((path, usess_id, RequestStatus.CANCELLED))


if __name__ == '__main__':
    class __App:
        def add_running_thread(self, name: str):
            pass
        def del_running_thread(self, name: str):
            pass
        _ncs_id = 'N/A'
        log = logging.getLogger()
        _logger = logging.getLogger()

    logging.basicConfig(level=logging.DEBUG)
    main(__App(), n_workers=2)
