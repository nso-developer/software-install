import datetime

import ncs

from .context import SoftwarePack
from .request_worker import RequestWorker


class CreateRequestAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        with ncs.maapi.single_write_trans('python-software-install-create-request',
                                          'system', db=ncs.OPERATIONAL) as oper_t_write:
            dsp = ncs.maagic.get_node(oper_t_write, kp)
            sp = ncs.maagic.get_node(oper_t_write, f'/software-install/software-pack{{{dsp.name}}}')
            target_sp = SoftwarePack.from_cdb(sp)

            update = False
            # TODO: change the requests list model to use timestamp?
            try:
                last_key = dsp.request.keys()[-1][0]
            except IndexError:
                last_key = 0
                update = True
                last_request_cancelled = False
            else:
                last_request = dsp.request[last_key]
                request_sp = SoftwarePack.from_cdb(last_request.software_pack_data)
                update = target_sp != request_sp
                last_request_cancelled = last_request.status == 'cancelled'

            if update or last_request_cancelled:
                # Obsolete all other requests
                for r in dsp.request:
                    r.obsolete = True

                # Create a new request with "target_sp" data
                key = int(last_key) + 1
                action_output.request_id = key
                r = dsp.request.create(key)
                target_sp.to_cdb(r.software_pack_data)
                action_output.status = 'new-request'

                self.log.debug(f'Creating new request {r._path} due to changes in software-pack')

                oper_t_write.apply()
            else:
                self.log.debug(f'Existing request {last_request._path} contains current software-pack data')
                action_output.request_id = last_key
                action_output.status = 'existing-request'


class ExecuteRequestAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        if name == 'execute-request':
            # TODO: "sync"-mode, where the action blocks until the request is
            # completed. Note: in "async" mode, where cb_action returns before
            # the request is done, it is not possible to write CLI messages
            RequestWorker.new_request(str(kp), usess_id=uinfo.usid)
            action_output.status = 'enqueued'
        elif name == 'cancel-request':
            RequestWorker.cancel_request(str(kp), usess_id=uinfo.usid)
            action_output.status = 'cancelled'


def _confirm_step(request, component, step, user):
    node = ncs.maagic.cd(request, f'component{{{component}}}/step{{{step}}}')
    node.confirmed.create()
    node.confirmed.by_user = user
    node.confirmed.when = datetime.datetime.now().isoformat()


class ConfirmStepAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        # TODO: when using a choice node, it is possible to check the case, like
        # action_input.step == 'all'. That doesn't work if the choice appears in
        # the ActionParams object though?! As a workaround (since cases are
        # mutually exclusive anyway), we first check for the existence of the
        # _all_ empty leaf.
        with ncs.maapi.Maapi() as m, \
             ncs.maapi.Session(m, 'python-sw-install-confirm-step', 'system'):
            with m.start_read_trans() as t_read:
                auto_execute = ncs.maagic.get_node(t_read, '/software-install/auto-execute-after-confirm')
            with m.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                request = ncs.maagic.get_node(oper_t_write, kp)
                if action_input.all.exists():
                    for c in request.component:
                        for s in c.step:
                            self.log.debug(f'{kp} confirming component {c.name} step {s.name}')
                            _confirm_step(request, c.name, s.name, uinfo.username)
                else:
                    for c in action_input.component:
                        for s in c.step:
                            self.log.debug(f'{kp} confirming component {c.name} step {s}')
                            _confirm_step(request, c.name, s, uinfo.username)
                oper_t_write.apply()
                if auto_execute:
                    request.execute_request()


class ClearRunLogAction(ncs.dp.Action):
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        with ncs.maapi.single_write_trans('python-software-install-clear-run-log', 'system', db=ncs.OPERATIONAL) as oper_t_write:
            oper_t_write.delete(f'{kp}/run-log')
            oper_t_write.apply()
        action_output.success = True


class SoftwareInstallActions(ncs.application.Application):
    def setup(self):
        self.register_action('software-install-create-request', CreateRequestAction)
        self.register_action('software-install-request-action', ExecuteRequestAction)
        self.register_action('software-install-confirm-step', ConfirmStepAction)
        self.register_action('software-install-clear-run-log', ClearRunLogAction)
