import time
from pathlib import Path
from typing import Iterable, Optional, Tuple, Type

from .context import Context, StateSros
from .software_install_script import NokiaSrosOperations, get_size, hash_file, CommunicationError
from .step_executor_classes import StepExecutor, StepExecutorSupportsPreCheck, StepResult


def get_steps(_: StateSros) -> Iterable[Type[StepExecutor]]:
    return (ValidatePlatform,
            CheckFiles,
            CheckVersions,
            ActivatePrimary,
            GetBootTime,
            CopyImage,
            PrepareCopyBootLdr,
            PrepareConfigureBof,
            PrepareHackFormatStandby,
            PrepareSaveRollback,
            PrepareSynchronizeBootenv,
            Reboot,
            Done)


def _get_device_ops(context: Context, state: StateSros) -> NokiaSrosOperations:
    return NokiaSrosOperations(context, state.device, vr=state.virtual_router)


class ValidatePlatform(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        o = _get_device_ops(self.context, state)
        result =o.validate_platform()
        if result:
            self.context.log.error(result)
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS

class CheckFiles(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        for f in state.software_pack_component.filenames:
            if not Path(f).is_file():
                self._add_oper_log(f'File {f} does not exist!')
                return StepResult.FAILURE
        return StepResult.SUCCESS


class CheckVersions(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        o = _get_device_ops(self.context, state)
        version = o.get_version()
        if state.done and state.version != version:
            # the target version was once running on this device (request was
            # done), but it has now changed?! let's assume we have to restart
            # the installation process
            state.reset()
        state.version = version
        state.virtual_router = o.is_virtual_router()
        if state.rebooted and state.boot_time:
            boot_time = o.get_boot_time()
            if boot_time > state.boot_time and state.version != state.software_pack_component.version:
                self.context.log.warning('Device rebooted, but new software version not active')
                return StepResult.FAILURE
        return StepResult.SUCCESS

    def next_step(self, state: StateSros) -> Optional[Type[StepExecutor]]:
        if state.version == state.software_pack_component.version:
            return Done
        elif state.rebooted:
            return Reboot
        else:
            return ActivatePrimary

class GetBootTime(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        if state.boot_time is None:
            o = _get_device_ops(self.context, state)
            state.boot_time = o.get_boot_time()
        return StepResult.SUCCESS


# TODO: these steps are "equal" on all platforms. Replace this copypasta?
class CopyImage(StepExecutor, StepExecutorSupportsPreCheck):
    def _is_file_present(self, state: StateSros, local_file: str) -> Tuple[bool, Optional[str]]:
        o = _get_device_ops(self.context, state)
        expected_size = get_size(local_file)
        assert(state.destination_volume is not None)
        try:
            files = o.list_files(state.destination_volume)
        except FileNotFoundError:
            return False, f'Folder {state.destination_volume} not found'

        filename = Path(local_file).name
        try:
            size = files[filename]
            if size != expected_size:
                return False, f'Expected size of {filename} {expected_size} does not match actual {size}'
        except KeyError:
            return False, f'File {filename} not found in {state.destination_volume}'

        try:
            remote_hash = o.calculate_checksum(str(Path(state.destination_volume) / filename))
        except NotImplementedError:
            pass
        else:
            local_hash = hash_file(local_file)
            if local_hash != remote_hash:
                return False, f'File {filename} hash {remote_hash} does not match expected {local_hash}'
        return True, None

    def pre_check(self, state: StateSros) -> StepResult:
        """Check if the image already exists, or there is enough available space for the image"""

        o = _get_device_ops(self.context, state)
        free_result = o.get_free_space()
        if free_result is None:
            self._add_oper_log('Could not get free space info from device')
            return StepResult.FAILURE

        if not state.destination_volume:
            state.destination_volume, free = free_result
            # the destination is a folder
            state.destination_volume = f'{state.destination_volume}/TiMOS-SR-{state.software_pack_component.version}'
        else:
            _, free = free_result


        missing = set(state.software_pack_component.filenames)
        errors = []
        for f in state.software_pack_component.filenames:
            ok, error = self._is_file_present(state, f)
            if ok:
                missing.remove(f)
                if f not in state.destination_paths:
                    state.destination_paths[f] = str(Path(state.destination_volume) / Path(f).name)
            else:
                state.destination_paths.pop(f, None)
                errors.append(error)
        if errors:
            self.context.log.debug(errors)
        else:
            return StepResult.SKIP_STEP

        total_size = sum(get_size(f) for f in state.software_pack_component.filenames if f in missing)
        # The file we're copying must fit into the destination partition. It
        # will be extracted elsewhere, so there is no need for a buffer.
        if total_size > free:
            self._add_oper_log(f'Not enough available space: image={total_size}, free={free}')
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateSros) -> StepResult:
        """Copy the image to destination"""
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)

        any_errors = False
        for f in state.software_pack_component.filenames:
            if f in state.destination_paths:
                continue

            state.destination_paths[f] = o.copy_image(f, state.destination_volume)

            present, error = self._is_file_present(state, f)
            if not present:
                assert(error is not None)
                self._add_oper_log(error)
                any_errors = True
        if any_errors:
            return StepResult.FAILURE
        else:
            return StepResult.SUCCESS


class ActivatePrimary(StepExecutor):
    def pre_check(self, state: StateSros) -> StepResult:
        o = _get_device_ops(self.context, state)
        active_node, redundancy_status = o.get_redundancy_info()
        if redundancy_status != 'standby':
            self.context.log.error(f"Redundancy status: '{redundancy_status}', not 'standby' (ready) as expected")
            return StepResult.FAILURE
        if active_node == 'A':
            return StepResult.SKIP_STEP
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateSros) -> StepResult:
        # If we reached the execute stage, "A" is not active
        o = _get_device_ops(self.context, state)
        o.redundancy_failover()

        # Important, now we have to block until standby is rebooted and running
        time.sleep(5)
        start = time.time()
        while time.time() - start < 600:
            try:
                active_node, redundancy_status = o.get_redundancy_info()
            except CommunicationError:
                pass
            else:
                if active_node == 'A' and redundancy_status == 'standby':
                    break
            time.sleep(10)
        else:
            self.context.log.error('Exceeded timeout waiting for standby (B) to reboot')
            return StepResult.FAILURE
        return StepResult.SUCCESS


class PrepareCopyBootLdr(StepExecutor):
    def pre_check(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)
        source_hash = o.calculate_checksum(f'{state.destination_volume}\\boot.ldr')
        try:
            destination_hash_active = o.calculate_checksum('cf3:\\boot.ldr')
            destination_hash_standby = o.calculate_checksum('cf3-b:\\boot.ldr')
        except Exception as err:
            if isinstance(err, NotImplementedError) or 'could not open file' in str(err):
                pass
        else:
            if source_hash == destination_hash_active or source_hash == destination_hash_standby:
                return StepResult.SKIP_STEP
        return StepResult.SUCCESS

    def execute(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)
        o.prepare_copy_bootldr(state.destination_volume)
        return StepResult.SUCCESS


class PrepareConfigureBof(StepExecutor):
    def pre_check(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)
        if o.is_in_sync() and o.is_bof_configured(state.destination_volume):
            return StepResult.SKIP_STEP
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)
        o.prepare_configure_bof(state.destination_volume)
        return StepResult.SUCCESS


class PrepareSaveRollback(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        # TODO: is it OK to execute this again? Proably overwrites previous snapshot??
        o = _get_device_ops(self.context, state)
        try:
            o.prepare_save_rollback(state.destination_volume)
        except NotImplementedError:
            return StepResult.SKIP_STEP
        return StepResult.SUCCESS


class PrepareSynchronizeBootenv(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        assert(state.destination_volume is not None)
        o = _get_device_ops(self.context, state)
        o.prepare_synchronize_bootenv()
        return StepResult.SUCCESS


class PrepareHackFormatStandby(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        o = _get_device_ops(self.context, state)
        try:
            o.prepare_format_standby()
        except NotImplementedError:
            return StepResult.SKIP_STEP
        return StepResult.SUCCESS


class Reboot(StepExecutor):
    def pre_check(self, state: StateSros) -> StepResult:
        if state.rebooted and state.boot_time:
            o = _get_device_ops(self.context, state)
            boot_time = o.get_boot_time()
            if boot_time > state.boot_time:
                # device has rebooted
                version = o.get_version()
                if version != state.software_pack_component.version:
                    self.context.log.warning(f'Device rebooted, but new software version not active. Expected {state.software_pack_component.version}, running {version}')
                    return StepResult.FAILURE
                else:
                    return StepResult.SKIP_STEP
            else:
                return StepResult.WAIT
        else:
            return StepResult.SUCCESS

    def execute(self, state: StateSros) -> StepResult:
        o = _get_device_ops(self.context, state)
        if o.reboot():
            state.rebooted = True
            return StepResult.WAIT
        else:
            return StepResult.FAILURE

class Done(StepExecutor):
    def execute(self, state: StateSros) -> StepResult:
        return StepResult.SUCCESS
