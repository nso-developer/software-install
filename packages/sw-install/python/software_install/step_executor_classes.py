
import abc
from enum import Enum, auto
from typing import Optional, Type

from typing_extensions import Protocol, runtime_checkable
from .context import Context


class StepResult(Enum):
    """Results from a single StepExecutor execution

    These are returned by the methods defined in the StepExecutor abstract class
    - execute() and pre_check()."""
    SUCCESS = auto()
    FAILURE = auto()
    # Skipped this step, execute the next
    SKIP_STEP = auto()
    # Skipped this step, do not execute any more steps for this component
    SKIP_COMPONENT = auto()
    # Step was executed successfully, but the device now needs some time to process
    WAIT = auto()


class StepExecutor(abc.ABC):
    @abc.abstractmethod
    def execute(self, state) -> StepResult:
        raise NotImplementedError()

    def next_step(self, _state) -> Optional[Type['StepExecutor']]:
        ...

    def __init__(self, context: Context):
        self.context = context

    def _add_oper_log(self, message: str):
        self.context.add_oper_log(message, step=self.name())

    @classmethod
    def name(cls) -> str:
        return cls.__qualname__


@runtime_checkable
class StepExecutorSupportsPreCheck(Protocol):
    def pre_check(self, state) -> StepResult:
        ...


class RequestStatus(Enum):
    UNPROCESSED = "unprocessed"
    WAITING_CONFIRMATION = "waiting-confirmation"
    PROCESSING = "processing"
    DONE = "done"
    CANCELLED = "cancelled"
    OBSOLETE = "obsolete"
    FAILED_OTHER = "failed-other"
    FAILED_TRANSIENT = "failed-transient"
