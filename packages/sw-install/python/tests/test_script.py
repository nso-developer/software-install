import unittest
import pathlib

from software_install.software_install_script import CiscoIosXrOperations, NokiaSrosCliStrategy


class TestCiscoIosXrOperations(unittest.TestCase):
    def test_log_package_extraction_added(self):
        log = """
Oct 09 08:56:03 Action 1: install add action started
Oct 09 08:56:09 WARNING : the following files/packages were not added to the Software Repository due to unsupported file format
                xrv9k-6.5.1.CSCvn28679.txt
Oct 09 08:56:10 Packages added:
Oct 09 08:56:10     xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679.x86_64
Oct 09 08:56:11 Action 1: install add action finished successfully
Oct 09 08:56:13 Install operation 1 finished successfully
Oct 09 08:56:13 Ending operation 1
"""

        packages = CiscoIosXrOperations.extract_package_info_from_log(log)
        self.assertListEqual(['xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679'], list(packages))

    def test_log_package_extraction_skipped(self):
        log = """
Oct 09 08:56:03 Action 1: install add action started
Oct 09 08:56:09 WARNING : the following files/packages were not added to the Software Repository due to unsupported file format
                xrv9k-6.5.1.CSCvn28679.txt
Oct 09 08:57:54 Packages skipped as they are already in the repository:
Oct 09 08:57:54     xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679.x86_64
Oct 09 08:56:11 Action 1: install add action finished successfully
Oct 09 08:56:13 Install operation 1 finished successfully
Oct 09 08:56:13 Ending operation 1
"""

        packages = CiscoIosXrOperations.extract_package_info_from_log(log)
        self.assertListEqual(['xrv9k-gcp-fwding-4.0.0.1-r651.CSCvn28679'], list(packages))

    def test_log_package_extraction_mixed(self):
        log = """
Oct 09 08:56:03 Action 1: install add action started
Oct 09 08:56:09 WARNING : the following files/packages were not added to the Software Repository due to unsupported file format
                xrv9k-6.5.1.CSCvn28679.txt
Oct 09 08:56:10 Packages added:
Oct 09 08:56:10     package1
Oct 09 08:57:54 Packages skipped as they are already in the repository:
Oct 09 08:57:54     package2
Oct 09 08:56:11 Action 1: install add action finished successfully
Oct 09 08:56:13 Install operation 1 finished successfully
Oct 09 08:56:13 Ending operation 1
"""

        packages = CiscoIosXrOperations.extract_package_info_from_log(log)
        self.assertListEqual(['package1', 'package2'], list(packages))

    def test_version_from_yaml(self):
        # the test .yml file is in the tests/ (current) folder
        folder = pathlib.Path(__file__).parent
        with open(folder / 'iosxr_image_mdata.yml') as yaml:
            version = CiscoIosXrOperations.version_from_yaml(yaml.read())
        self.assertEqual(version, 'xrv9k-xr-6.5.2')


class TestNokiaSrosOperations(unittest.TestCase):
    def test_uptime_convert(self):
        uptime = NokiaSrosCliStrategy._uptime_to_seconds('0 days, 01:15:29.42')
        self.assertEqual(uptime, 4529)
