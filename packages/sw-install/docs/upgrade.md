# Software upgrade

The upgrade or software installation process can be roughly divided into three stages:
- prepare
- activate,
- commit.

Each stage contains one or more steps, which differ depending on the
platform and the type of software package. A general description of steps
without referencing specific platforms:

- prepare:
    - determine the destination location on the device (multiple filesystems?)
    - copy the software to the device
    - validate the software package (checksum or command)
    - any other actions that we can perform that won't affect the running software, even after a reboot
- activate:
    - activate the prepared software
    - reboot if needed
- commit:
    - persist the setting for boot image
    - remove the old software from filesystem

In the following sections, we detail the step interface and define the
steps specific to each platform.

## Step executor
An individual `StepExecutor` wraps an atomic imperative device action.
A `StepExecutor` must define at least the `execute()` method:

```python
class StepExecutor(abc.ABC):
    @abc.abstractmethod
    def execute(self, state: State) -> bool: ...
```

The `post_check()` and `pre_check()` methods are optional:

```python
class StepExecutorSupportsPreCheck(Protocol):
    def pre_check(self, state: State) -> bool: ...


class StepExecutorSupportsPostCheck(Protocol):
    def post_check(self, state: State) -> bool: ...
```

Every method in the `StepExecutor` returns a boolean success value
TODO: change to success/fail/pass?

The input to the methods is a mutable `State` object. The fields are populated
by the `StepExecutor` methods, with the values also being persisted.
For instance, the `CopyImage` action checks for available disk space and sets
the `state.destination` attribute to the path with the most free space
available. This is then used in the later steps for software activation.

## Step processor
The list of steps for upgrading to a new software pack can be:
1. generated once, then persisted to CDB,
2. generated on the fly, as the processor picks up the task for the device.

### Steps generated once, then persisted to CDB
The list of steps for installing a software pack is generated once, with all
steps set to `not-reached` status. The list of steps represents the execution
plan for the processor, and also contains the operational data for tracking
progress (status, logs, ...). The processor picks up the first
`not-reached` and executes it. If the steps require operator confirmation, a
flag is set (??).

If the software pack configuration changes, this approach makes it more
diffucult to update the list of steps. Which ones are still required?
What happens with steps that weren't executed?

### Steps generated dynamically
The list of steps is generated on the fly. Once the processor starts executing
the upgrade for a device, it retrieves a list of steps based on the current state
of the device. The first step checks if the software is already installed. If
it is, the step outputs a *all-done* result, which results in all following
steps being marked as skipped If the software is to be installed, the steps are
executed in order. The results of the step execution are still written to CDB,
but aren't used as instructions to the processor in the next iteration.

## IOS XR

All install operations on IOS XR are asynchronous. Every RPC returns an
`op-id`, which must then be used to check progress of the command in the
install log (unstructured text).

### Prepare

#### `CopyImage`
Copies the software image via SCP to the filesystem with the most availabile space.
**This must be always executed as there isn't a way of getting the list of
files on the device**
- [ ] complain it is not possible to list files / checksum file via NETCONF
- [ ] We can probably use SFTP to list files & get size at least ...


- `pre_check`: Check for available space on the filesystem. *Output: `destination`*
- `execute`: Always copy the image to `state.destination`
- `post_check`: pass

#### `AddSoftware`
Adds the software image to the package repository
- `pre_check`: Check if software was already added (shows up in inactive-summary), possibly skip execute.
- `execute`: Run `install add`. *Output: `add-operation-id`*
- `post_check` Check if software was added (shows up in inactive-summary)

#### `PrepareSoftware`
Unpack the software image, speeds up the activate stage.
- `pre_check`: Check if software was already prepared (shows up in prepared-boot-image), possibly skip execute.
  - [ ] this works for base image, what about SMU?
- `execute`: Run `install prepare` on `add-operation-id` from previous step. *Output: `prepare-operation-id`*
- `post_check`: Check if software was prepared (shows up in prepared-boot-image)

### Activate

#### `ActivateSoftware`
Start using the software (implicit reboot)
- `pre_check`: ?
- `execute`: Run `install activate` on previous operation
- `post_check`: Check if new software is running

### Commit

#### `CommitSoftware`
Commit the currently active software config
- `pre_check`: Check if target software version is running
- `execute`: Run `install commit`
- `post_check`: ??

## Junos

### Prepare

#### `CopyImage`
Copies the software image to the device
- `pre_check`: Check for available space on the filesystem. *Output: `destination`*
- `execute`: Copy the image to `state.destination`
- `post_check`: Verify checksum of the copied image on the device (RPC exists)

### Activate / Commit
By adding the software package, it is implicitly set to load on next reboot.

#### `PackageAdd`
Adds the package to the device repository. **The package is also marked as active
implicitly and will be used if the device reboots for whatever reason.**
- `pre_check`: Check if package wasn't already added
  - [ ] is there an RPC?
- `execute`: Run `request package add`
- `post_check`: ??

#### `Reboot`
Explicit reboot of the device to activate the new software.
- `pre_check`: Check if the expected software is already running
- `execute`: Run `request reboot`
- `post_check`: ??

## SROS

### Prepare

#### `CopyImage`
Copies the software image folder to the device. The folder contains multiple files.
- `pre_check`: Check for available space on the filesystem. *Output: `destination`*
- `execute`: Copy the software folder to `state.destination`
- `post_check`: Verify files exist on the destination and have correct size

#### Activate / Commit
By replacing the bootloader and configuring BOF, we're effectively "committing"
the change as well.

#### `ReplaceBootLoader`
Replaces the `boot.ldr` in the filesystem root with the new version.
- `pre_check`: ??
- `execute`: Copy `boot.ldr` from `state.destination` to filesystem root.
- `post_check`: Verify file sizes match

#### `SwitchPrimaryImageConfig`
Changes BOF `primary-image` to the new version, and `secondary-image` to the
currently configured version in `primary-image`.
- `pre_check`: ??
- `execute`: Reconfigure BOF
- `post_check`: ??

#### `SaveRollbackConfig`
Executes `admin save rollback rescue` before upgrade.
- `pre_check`: ??
- `execute`: Run the `admin save rollback rescue comment "Pre-upgrade"` command
  - [ ] figure out how to send the comment (some problem with escaping)
- `post_check`: ??

#### `Reboot`
Explicit reboot of the device to activate the new software.
- `pre_check`: Check if the expected software is already running
- `execute`: Run `admin reboot upgrade now`
- `post_check`: ??